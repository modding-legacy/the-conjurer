package com.legacy.conjurer_illager.client;

import com.legacy.conjurer_illager.TCRegistry;

import net.minecraft.client.model.HumanoidModel;
import net.minecraft.client.model.Model;
import net.minecraft.client.renderer.entity.state.HumanoidRenderState;
import net.minecraft.client.resources.model.EquipmentClientInfo;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.neoforged.neoforge.client.extensions.common.IClientItemExtensions;
import net.neoforged.neoforge.client.extensions.common.RegisterClientExtensionsEvent;

public class TCClientEvents
{
	public static void registerClientExtensions(RegisterClientExtensionsEvent event)
	{
		Item hat = TCRegistry.ItemReg.CONJURER_HAT.asItem();

		if (!event.isItemRegistered(hat))
			event.registerItem(new IClientItemExtensions()
			{
				@Override
				public HumanoidModel<?> getHumanoidArmorModel(ItemStack itemStack, EquipmentClientInfo.LayerType layerType, Model original)
				{
					return new com.legacy.conjurer_illager.client.model.ConjurerHatModel<HumanoidRenderState>(net.minecraft.client.Minecraft.getInstance().getEntityModels().bakeLayer(com.legacy.conjurer_illager.client.IllagerRenderRefs.CONJURER_HAT));
				}
			}, hat);
	}
}
