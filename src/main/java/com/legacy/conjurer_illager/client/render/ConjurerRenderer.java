package com.legacy.conjurer_illager.client.render;

import com.legacy.conjurer_illager.TheConjurerMod;
import com.legacy.conjurer_illager.client.IllagerRenderRefs;
import com.legacy.conjurer_illager.client.model.ConjurerModel;
import com.legacy.conjurer_illager.client.render.state.ConjurerRenderState;
import com.legacy.conjurer_illager.entity.ConjurerEntity;
import com.mojang.blaze3d.vertex.PoseStack;

import net.minecraft.client.model.IllagerModel;
import net.minecraft.client.renderer.MultiBufferSource;
import net.minecraft.client.renderer.entity.EntityRendererProvider;
import net.minecraft.client.renderer.entity.IllagerRenderer;
import net.minecraft.client.renderer.entity.layers.ItemInHandLayer;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.entity.monster.AbstractIllager.IllagerArmPose;
import net.neoforged.api.distmarker.Dist;
import net.neoforged.api.distmarker.OnlyIn;

@OnlyIn(Dist.CLIENT)
public class ConjurerRenderer<T extends ConjurerEntity> extends IllagerRenderer<T, ConjurerRenderState>
{
	private static final ResourceLocation TEXTURE = TheConjurerMod.locate("textures/entity/conjurer_illager.png");

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public ConjurerRenderer(EntityRendererProvider.Context context)
	{
		super(context, new ConjurerModel<>(context.bakeLayer(IllagerRenderRefs.CONJURER)), 0.5F);
		this.addLayer(new ConjurerSunglassesLayer(this, new ConjurerModel<>(context.bakeLayer(IllagerRenderRefs.CONJURER_GLASSES))));
		this.addLayer(new ItemInHandLayer<ConjurerRenderState, IllagerModel<ConjurerRenderState>>(this)
		{
			@Override
			public void render(PoseStack pose, MultiBufferSource buffer, int packedLight, ConjurerRenderState state, float p_114551_, float p_114552_)
			{
				if (state.armPose == IllagerArmPose.CROSSBOW_HOLD)
					super.render(pose, buffer, packedLight, state, p_114551_, p_114552_);
			}
		});
	}

	@Override
	public ResourceLocation getTextureLocation(ConjurerRenderState renderState)
	{
		return TEXTURE;
	}

	@Override
	public void extractRenderState(T entity, ConjurerRenderState state, float partialTicks)
	{
		super.extractRenderState(entity, state, partialTicks);
		state.isCastingSpell = entity.isCastingSpell();

		// state.armPose = IllagerArmPose.CELEBRATING;
	}

	@Override
	public ConjurerRenderState createRenderState()
	{
		return new ConjurerRenderState();
	}
}
