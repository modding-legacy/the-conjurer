package com.legacy.conjurer_illager.client.render;

import com.legacy.conjurer_illager.client.IllagerRenderRefs;
import com.legacy.conjurer_illager.client.model.ConjurerHatModel;
import com.legacy.conjurer_illager.client.model.ConjurerModel;
import com.legacy.conjurer_illager.client.model.ThrowingCardModel;
import com.legacy.conjurer_illager.registry.TCEntityTypes;

import net.minecraft.client.model.geom.builders.CubeDeformation;
import net.neoforged.bus.api.IEventBus;
import net.neoforged.neoforge.client.event.EntityRenderersEvent;

public class TCEntityRendering
{
	public static void init(IEventBus modBus)
	{
		modBus.addListener(TCEntityRendering::initLayers);
		modBus.addListener(TCEntityRendering::initRenders);
	}

	public static void initLayers(EntityRenderersEvent.RegisterLayerDefinitions event)
	{
		event.registerLayerDefinition(IllagerRenderRefs.CONJURER, () -> ConjurerModel.createBodyLayer(CubeDeformation.NONE));
		event.registerLayerDefinition(IllagerRenderRefs.CONJURER_GLASSES, () -> ConjurerModel.createBodyLayer(new CubeDeformation(0.3F)));

		event.registerLayerDefinition(IllagerRenderRefs.THROWING_CARD, () -> ThrowingCardModel.createLayer());

		event.registerLayerDefinition(IllagerRenderRefs.CONJURER_HAT, () -> ConjurerHatModel.createLayer());
	}

	public static void initRenders(EntityRenderersEvent.RegisterRenderers event)
	{
		event.registerEntityRenderer(TCEntityTypes.CONJURER, ConjurerRenderer::new);
		event.registerEntityRenderer(TCEntityTypes.THROWING_CARD, ThrowingCardRenderer::new);
		event.registerEntityRenderer(TCEntityTypes.BOUNCING_BALL, BouncingBallRenderer::new);
	}
}