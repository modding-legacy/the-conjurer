package com.legacy.conjurer_illager.client.render;

import com.legacy.conjurer_illager.TheConjurerMod;
import com.legacy.conjurer_illager.client.render.state.BouncingBallRenderState;
import com.legacy.conjurer_illager.entity.BouncingBallEntity;
import com.mojang.blaze3d.vertex.PoseStack;
import com.mojang.blaze3d.vertex.VertexConsumer;

import net.minecraft.client.renderer.MultiBufferSource;
import net.minecraft.client.renderer.RenderType;
import net.minecraft.client.renderer.entity.EntityRenderer;
import net.minecraft.client.renderer.entity.EntityRendererProvider;
import net.minecraft.client.renderer.texture.OverlayTexture;
import net.minecraft.resources.ResourceLocation;
import net.neoforged.api.distmarker.Dist;
import net.neoforged.api.distmarker.OnlyIn;

@OnlyIn(Dist.CLIENT)
public class BouncingBallRenderer<T extends BouncingBallEntity> extends EntityRenderer<T, BouncingBallRenderState>
{
	private static final ResourceLocation[] BALLS = new ResourceLocation[] { TheConjurerMod.locate("textures/entity/bouncy_ball/stripe_1.png"), TheConjurerMod.locate("textures/entity/bouncy_ball/stripe_2.png"), TheConjurerMod.locate("textures/entity/bouncy_ball/stripes_1.png"), TheConjurerMod.locate("textures/entity/bouncy_ball/stripes_2.png"), TheConjurerMod.locate("textures/entity/bouncy_ball/spots_1.png"), TheConjurerMod.locate("textures/entity/bouncy_ball/spots_2.png") };
	private static final RenderType[] RENDER_TYPES = new RenderType[] { RenderType.entityCutoutNoCull(BALLS[0]), RenderType.entityCutoutNoCull(BALLS[1]), RenderType.entityCutoutNoCull(BALLS[2]), RenderType.entityCutoutNoCull(BALLS[3]), RenderType.entityCutoutNoCull(BALLS[4]), RenderType.entityCutoutNoCull(BALLS[5]) };

	public BouncingBallRenderer(EntityRendererProvider.Context context)
	{
		super(context);
	}

	@Override
	public void render(BouncingBallRenderState state, PoseStack pose, MultiBufferSource source, int packedLight)
	{
		pose.pushPose();

		float size = 0.8F;
		pose.scale(size, size, size);
		pose.mulPose(this.entityRenderDispatcher.cameraOrientation());
		PoseStack.Pose posestack$pose = pose.last();
		VertexConsumer vertexconsumer = source.getBuffer(RENDER_TYPES[state.ballType]);
		vertex(vertexconsumer, posestack$pose, packedLight, 0.0F, 0, 0, 1);
		vertex(vertexconsumer, posestack$pose, packedLight, 1.0F, 0, 1, 1);
		vertex(vertexconsumer, posestack$pose, packedLight, 1.0F, 1, 1, 0);
		vertex(vertexconsumer, posestack$pose, packedLight, 0.0F, 1, 0, 0);
		pose.popPose();
		super.render(state, pose, source, packedLight);
	}

	private static void vertex(VertexConsumer consumer, PoseStack.Pose pose, int packedLight, float x, int y, int u, int v)
	{
		consumer.addVertex(pose, x - 0.5F, (float) y - 0.25F, 0.0F).setColor(-1).setUv((float) u, (float) v).setOverlay(OverlayTexture.NO_OVERLAY).setLight(packedLight).setNormal(pose, 0.0F, 1.0F, 0.0F);
	}

	@Override
	public void extractRenderState(T entity, BouncingBallRenderState state, float partialTicks)
	{
		super.extractRenderState(entity, state, partialTicks);
		state.ballType = entity.getBallType();
	}

	@Override
	public BouncingBallRenderState createRenderState()
	{
		return new BouncingBallRenderState();
	}
}