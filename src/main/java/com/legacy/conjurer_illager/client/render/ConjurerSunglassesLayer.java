package com.legacy.conjurer_illager.client.render;

import java.util.List;

import com.legacy.conjurer_illager.TheConjurerMod;
import com.legacy.conjurer_illager.client.model.ConjurerModel;
import com.mojang.blaze3d.vertex.PoseStack;
import com.mojang.blaze3d.vertex.VertexConsumer;

import net.minecraft.client.model.ArmedModel;
import net.minecraft.client.renderer.MultiBufferSource;
import net.minecraft.client.renderer.RenderType;
import net.minecraft.client.renderer.entity.RenderLayerParent;
import net.minecraft.client.renderer.entity.layers.RenderLayer;
import net.minecraft.client.renderer.entity.state.IllagerRenderState;
import net.minecraft.client.renderer.texture.OverlayTexture;
import net.minecraft.resources.ResourceLocation;
import net.neoforged.api.distmarker.Dist;
import net.neoforged.api.distmarker.OnlyIn;

@OnlyIn(Dist.CLIENT)
public class ConjurerSunglassesLayer<S extends IllagerRenderState, M extends ConjurerModel<S> & ArmedModel> extends RenderLayer<S, M>
{
	private static final List<String> VAM_NAMES = List.of("Vamacheron", "Vam", "Vamacher0n");
	private static final ResourceLocation TEXTURE = TheConjurerMod.locate("textures/entity/sunglasses.png");
	private final M model;

	public ConjurerSunglassesLayer(RenderLayerParent<S, M> renderer, M model)
	{
		super(renderer);
		this.model = model;
	}

	@Override
	public void render(PoseStack pose, MultiBufferSource buffer, int p_117396_, S state, float p_117398_, float p_117399_)
	{
		if (!state.isInvisible && state.customName != null && VAM_NAMES.contains(state.customName.getString()))
		{
			this.model.setupAnim(state);
			VertexConsumer vertexconsumer = buffer.getBuffer(RenderType.entityTranslucent(TEXTURE));
			this.model.renderToBuffer(pose, vertexconsumer, p_117396_, OverlayTexture.NO_OVERLAY);
		}
	}
}
