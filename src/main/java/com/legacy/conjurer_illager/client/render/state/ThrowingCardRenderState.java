package com.legacy.conjurer_illager.client.render.state;

import net.minecraft.client.renderer.entity.state.EntityRenderState;

public class ThrowingCardRenderState extends EntityRenderState
{
	public int cardType;
	public float yRot;
}
