package com.legacy.conjurer_illager.client.render;

import com.legacy.conjurer_illager.TheConjurerMod;
import com.legacy.conjurer_illager.client.IllagerRenderRefs;
import com.legacy.conjurer_illager.client.model.ThrowingCardModel;
import com.legacy.conjurer_illager.client.render.state.ThrowingCardRenderState;
import com.legacy.conjurer_illager.entity.ThrowingCardEntity;
import com.mojang.blaze3d.vertex.PoseStack;
import com.mojang.blaze3d.vertex.VertexConsumer;
import com.mojang.math.Axis;

import net.minecraft.client.renderer.MultiBufferSource;
import net.minecraft.client.renderer.entity.EntityRenderer;
import net.minecraft.client.renderer.entity.EntityRendererProvider;
import net.minecraft.client.renderer.texture.OverlayTexture;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.util.Mth;
import net.neoforged.api.distmarker.Dist;
import net.neoforged.api.distmarker.OnlyIn;

@OnlyIn(Dist.CLIENT)
public class ThrowingCardRenderer<T extends ThrowingCardEntity> extends EntityRenderer<T, ThrowingCardRenderState>
{
	private static final ResourceLocation[] CARDS = new ResourceLocation[] { TheConjurerMod.locate("textures/entity/throwing_card/red_throwing_card.png"), TheConjurerMod.locate("textures/entity/throwing_card/black_throwing_card.png"), TheConjurerMod.locate("textures/entity/throwing_card/blue_throwing_card.png") };
	private final ThrowingCardModel model;

	public ThrowingCardRenderer(EntityRendererProvider.Context context)
	{
		super(context);

		this.model = new ThrowingCardModel(context.bakeLayer(IllagerRenderRefs.THROWING_CARD));
	}

	@Override
	public void render(ThrowingCardRenderState state, PoseStack pose, MultiBufferSource buffer, int packedLight)
	{
		pose.pushPose();
		pose.translate(0.0D, (double) -0.23F, 0.0D);
		pose.scale(0.8F, 0.2F, 0.8F);
		pose.mulPose(Axis.YP.rotationDegrees(state.yRot));
		VertexConsumer ivertexbuilder = buffer.getBuffer(model.renderType(CARDS[state.cardType]));
		model.renderToBuffer(pose, ivertexbuilder, packedLight, OverlayTexture.NO_OVERLAY);
		pose.popPose();
		super.render(state, pose, buffer, packedLight);
	}

	@Override
	public void extractRenderState(T entity, ThrowingCardRenderState state, float partialTicks)
	{
		super.extractRenderState(entity, state, partialTicks);
		state.cardType = entity.getCardType();
		state.yRot = Mth.lerp(partialTicks, entity.yRotO, entity.getYRot());
	}

	@Override
	public ThrowingCardRenderState createRenderState()
	{
		return new ThrowingCardRenderState();
	}
}