package com.legacy.conjurer_illager.client.model;

import net.minecraft.client.model.HumanoidModel;
import net.minecraft.client.model.geom.ModelPart;
import net.minecraft.client.model.geom.PartPose;
import net.minecraft.client.model.geom.builders.CubeDeformation;
import net.minecraft.client.model.geom.builders.CubeListBuilder;
import net.minecraft.client.model.geom.builders.LayerDefinition;
import net.minecraft.client.model.geom.builders.MeshDefinition;
import net.minecraft.client.model.geom.builders.PartDefinition;
import net.minecraft.client.renderer.entity.state.HumanoidRenderState;

public class ConjurerHatModel<T extends HumanoidRenderState> extends HumanoidModel<T>
{
	protected final ModelPart hatTop;
	protected final ModelPart hatBottom;

	public ConjurerHatModel(ModelPart model)
	{
		super(model);

		this.hatTop = this.head.getChild("hat_top");
		this.hatBottom = this.head.getChild("hat_bottom");
	}

	public static LayerDefinition createLayer()
	{
		MeshDefinition mesh = HumanoidModel.createMesh(CubeDeformation.NONE, 0.0F);
		PartDefinition root = mesh.getRoot();

		PartDefinition head = root.getChild("head");

		head.addOrReplaceChild("hat_top", CubeListBuilder.create().texOffs(96, 0).addBox(-4.0F, -15.0F, -4.0F, 8, 8, 8, new CubeDeformation(0.01F)), PartPose.ZERO);
		head.addOrReplaceChild("hat_bottom", CubeListBuilder.create().texOffs(84, 16).addBox(-5.5F, -7.0F, -5.5F, 11, 2, 11, new CubeDeformation(0.01F)), PartPose.ZERO);

		return LayerDefinition.create(mesh, 128, 64);
	}

	@Override
	public ModelPart getHead()
	{
		return super.getHead();
	}

	@Override
	public void setupAnim(T context)
	{
		super.setupAnim(context);
	}

}
