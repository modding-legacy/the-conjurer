package com.legacy.conjurer_illager.client;

import java.util.Optional;

import com.legacy.conjurer_illager.TheConjurerMod;

import net.minecraft.network.chat.Component;
import net.minecraft.network.chat.MutableComponent;
import net.minecraft.server.packs.PackLocationInfo;
import net.minecraft.server.packs.PackSelectionConfig;
import net.minecraft.server.packs.PackType;
import net.minecraft.server.packs.PathPackResources;
import net.minecraft.server.packs.repository.BuiltInPackSource;
import net.minecraft.server.packs.repository.Pack;
import net.minecraft.server.packs.repository.PackSource;
import net.neoforged.fml.ModList;
import net.neoforged.neoforgespi.locating.IModFile;

public class TCResourcePackHandler
{
	public static final Pack LEGACY_PACK = createPack(Component.literal("The Conjurer Programmer Art"), "legacy_pack").hidden();

	public static Pack createPack(MutableComponent name, String folder)
	{
		IModFile file = ModList.get().getModFileById(modid()).getFile();

		PackLocationInfo loc = new PackLocationInfo(modid() + ":" + folder, name, PackSource.BUILT_IN, Optional.empty());
		PathPackResources packResources = new PathPackResources(loc, file.findResource("assets/" + modid() + "/" + folder));
		return Pack.readMetaAndCreate(loc, BuiltInPackSource.fixedResources(packResources), PackType.CLIENT_RESOURCES, new PackSelectionConfig(false, Pack.Position.TOP, false));
	}

	private static String modid()
	{
		return TheConjurerMod.MODID;
	}
}