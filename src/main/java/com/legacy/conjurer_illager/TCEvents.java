package com.legacy.conjurer_illager;

import com.legacy.conjurer_illager.data.TCTags;
import com.legacy.conjurer_illager.entity.ConjurerEntity;
import com.legacy.conjurer_illager.registry.TCStructures;

import net.minecraft.sounds.SoundEvents;
import net.minecraft.tags.DamageTypeTags;
import net.minecraft.world.effect.MobEffects;
import net.minecraft.world.entity.EquipmentSlot;
import net.minecraft.world.entity.animal.Rabbit;
import net.minecraft.world.entity.animal.Rabbit.Variant;
import net.minecraft.world.entity.npc.VillagerProfession;
import net.minecraft.world.entity.npc.VillagerTrades;
import net.minecraft.world.level.saveddata.maps.MapDecorationTypes;
import net.neoforged.bus.api.SubscribeEvent;
import net.neoforged.neoforge.event.entity.living.LivingDamageEvent;
import net.neoforged.neoforge.event.entity.living.LivingIncomingDamageEvent;
import net.neoforged.neoforge.event.entity.living.MobEffectEvent;
import net.neoforged.neoforge.event.village.VillagerTradesEvent;

public class TCEvents
{
	@SubscribeEvent
	public static void onPotionEffectGiven(MobEffectEvent.Applicable event)
	{
		if (event.getEffectInstance().getEffect() == MobEffects.BAD_OMEN && event.getEntity().getItemBySlot(EquipmentSlot.HEAD).is(TCRegistry.ItemReg.CONJURER_HAT.get()))
			event.setResult(MobEffectEvent.Applicable.Result.DO_NOT_APPLY);
	}

	@SubscribeEvent
	public static void onLivingAttack(LivingIncomingDamageEvent event)
	{
		if (event.getAmount() > 0 && event.getSource().getDirectEntity() instanceof Rabbit rabbit && !rabbit.level().isClientSide && rabbit.getVariant() != Variant.EVIL)
			rabbit.playSound(SoundEvents.RABBIT_ATTACK, 1.0F, (rabbit.getRandom().nextFloat() - rabbit.getRandom().nextFloat()) * 0.2F + 1.0F);
	}

	@SubscribeEvent
	public static void onLivingDamage(LivingDamageEvent.Pre event)
	{
		if (event.getSource().is(DamageTypeTags.WITCH_RESISTANT_TO) && (event.getEntity().getItemBySlot(EquipmentSlot.HEAD).is(TCRegistry.ItemReg.CONJURER_HAT.get()) || event.getEntity() instanceof ConjurerEntity))
			event.setNewDamage(event.getOriginalDamage() * 0.70F);
	}

	@SubscribeEvent
	public static void onVillagerTradesAssigned(VillagerTradesEvent event)
	{
		try
		{
			if (event.getType() == VillagerProfession.CARTOGRAPHER)
				event.getTrades().get(2).add(new VillagerTrades.TreasureMapForEmeralds(11, TCTags.Structures.ON_THEATER_MAPS, TCStructures.getTheaterMapKey(), MapDecorationTypes.RED_X, 15, 5));
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}

	}
}