package com.legacy.conjurer_illager.registry;

import java.util.Locale;

import com.legacy.conjurer_illager.TheConjurerMod;
import com.legacy.conjurer_illager.world.TheaterPools;
import com.legacy.conjurer_illager.world.TheaterStructure;
import com.legacy.structure_gel.api.registry.registrar.RegistrarHandler;
import com.legacy.structure_gel.api.registry.registrar.StructureRegistrar;
import com.legacy.structure_gel.api.structure.ExtendedJigsawStructure;
import com.legacy.structure_gel.api.structure.GridStructurePlacement;

import net.minecraft.core.registries.Registries;
import net.minecraft.world.level.biome.Biomes;
import net.minecraft.world.level.levelgen.Heightmap;
import net.minecraft.world.level.levelgen.structure.StructureSpawnOverride;
import net.minecraft.world.level.levelgen.structure.TerrainAdjustment;
import net.minecraft.world.level.levelgen.structure.pools.StructureTemplatePool;

public class TCStructures
{
	public static final RegistrarHandler<StructureTemplatePool> HANDLER = RegistrarHandler.getOrCreate(Registries.TEMPLATE_POOL, TheConjurerMod.MODID).bootstrap(TheaterPools::bootstrap);

	// @formatter:off
	public static final StructureRegistrar<ExtendedJigsawStructure> THEATER = StructureRegistrar.jigsawBuilder(TheConjurerMod.locate("theatre"))
			.addPiece(() -> TheaterStructure.Piece::new)
			.pushStructure((c, s) -> ExtendedJigsawStructure.builder(s, c.lookup(Registries.TEMPLATE_POOL).getOrThrow(TheaterPools.ROOT)).heightmap(Heightmap.Types.WORLD_SURFACE_WG).capability(TheaterStructure.Capability.INSTANCE).build())
				.terrainAdjustment(TerrainAdjustment.BEARD_THIN).noSpawns(StructureSpawnOverride.BoundingBoxType.PIECE).biomes(Biomes.DARK_FOREST)
			.popStructure()
			.placement(() -> GridStructurePlacement.builder(40, 35, 0.50F).build(TCStructures.THEATER.getRegistryName()))
			.build();
	// @formatter:on

	public static String getTheaterMapKey()
	{
		return "filled_map." + THEATER.getRegistryName().toString().toLowerCase(Locale.ROOT);
	}

	public static void init()
	{
	}
}