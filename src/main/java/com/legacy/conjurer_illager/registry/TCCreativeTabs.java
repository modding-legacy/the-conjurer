package com.legacy.conjurer_illager.registry;

import com.legacy.conjurer_illager.TCRegistry;
import com.legacy.conjurer_illager.TheConjurerMod;

import net.minecraft.world.item.CreativeModeTabs;
import net.neoforged.bus.api.SubscribeEvent;
import net.neoforged.fml.common.EventBusSubscriber;
import net.neoforged.fml.common.EventBusSubscriber.Bus;
import net.neoforged.neoforge.event.BuildCreativeModeTabContentsEvent;

@EventBusSubscriber(modid = TheConjurerMod.MODID, bus = Bus.MOD)
public class TCCreativeTabs
{
	@SubscribeEvent
	public static void modifyExisting(BuildCreativeModeTabContentsEvent event)
	{
		if (event.getTabKey() == CreativeModeTabs.SPAWN_EGGS)
			event.accept(TCRegistry.ItemReg.CONJURER_SPAWN_EGG);

		if (event.getTabKey() == CreativeModeTabs.COMBAT)
		{
			event.accept(TCRegistry.ItemReg.CONJURER_HAT);
			event.accept(TCRegistry.ItemReg.THROWABLE_BALL);
			event.accept(TCRegistry.ItemReg.THROWING_CARD);
		}

		if (event.getTabKey() == CreativeModeTabs.TOOLS_AND_UTILITIES)
			event.accept(TCRegistry.ItemReg.DELVE_DEEPER_RECORD);
	}
}
