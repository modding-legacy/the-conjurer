package com.legacy.conjurer_illager.registry;

import com.legacy.conjurer_illager.TheConjurerMod;
import com.legacy.conjurer_illager.client.particle.PlayingCardParticle;

import net.minecraft.core.particles.SimpleParticleType;
import net.minecraft.core.registries.Registries;
import net.neoforged.bus.api.SubscribeEvent;
import net.neoforged.neoforge.registries.RegisterEvent;

public class TCParticles
{
	public static final SimpleParticleType RED_PLAYING_CARD = new SimpleParticleType(false);
	public static final SimpleParticleType BLACK_PLAYING_CARD = new SimpleParticleType(false);

	public static void init(RegisterEvent event)
	{
		event.register(Registries.PARTICLE_TYPE, TheConjurerMod.locate("red_playing_card"), () -> RED_PLAYING_CARD);
		event.register(Registries.PARTICLE_TYPE, TheConjurerMod.locate("black_playing_card"), () -> BLACK_PLAYING_CARD);
	}

	public static class Register
	{
		@SubscribeEvent
		public static void registerParticleFactories(net.neoforged.neoforge.client.event.RegisterParticleProvidersEvent event)
		{
			event.registerSpriteSet(RED_PLAYING_CARD, PlayingCardParticle.Factory::new);
			event.registerSpriteSet(BLACK_PLAYING_CARD, PlayingCardParticle.Factory::new);
		}
	}
}
