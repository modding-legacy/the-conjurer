package com.legacy.conjurer_illager.registry;

import com.legacy.conjurer_illager.TheConjurerMod;
import com.legacy.conjurer_illager.world.TheaterStructure;
import com.legacy.structure_gel.api.registry.StructureGelRegistries;
import com.legacy.structure_gel.api.structure.jigsaw.JigsawCapabilityType;

import net.neoforged.neoforge.registries.RegisterEvent;

public class TCJigsawTypes
{
	public static final JigsawCapabilityType<TheaterStructure.Capability> THEATER = () -> TheaterStructure.Capability.CODEC;

	public static void init(final RegisterEvent event)
	{
		register(event, "theater", THEATER);
	}

	private static void register(RegisterEvent event, String key, JigsawCapabilityType<?> type)
	{
		event.register(StructureGelRegistries.Keys.JIGSAW_TYPE, TheConjurerMod.locate(key), () -> type);
	}
}
