package com.legacy.conjurer_illager.registry;

import com.legacy.conjurer_illager.TheConjurerMod;

import net.minecraft.core.Holder;
import net.minecraft.core.Registry;
import net.minecraft.core.registries.BuiltInRegistries;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.sounds.SoundEvent;

public interface TCSounds
{
	SoundEvent ENTITY_CONJURER_IDLE = create("entity.conjurer.idle");
	SoundEvent ENTITY_CONJURER_HURT = create("entity.conjurer.hurt");
	SoundEvent ENTITY_CONJURER_DEATH = create("entity.conjurer.death");
	SoundEvent ENTITY_CONJURER_CELEBRATE = create("entity.conjurer.celebrate");

	SoundEvent ENTITY_CONJURER_CAST_SPELL = create("entity.conjurer.cast_spell");
	SoundEvent ENTITY_CONJURER_DISAPPEAR = create("entity.conjurer.disappear");
	SoundEvent ENTITY_CONJURER_PREPARE_VANISH = create("entity.conjurer.prepare_vanish");
	SoundEvent ENTITY_CONJURER_PREPARE_RABBIT = create("entity.conjurer.prepare_rabbit");
	SoundEvent ENTITY_CONJURER_PREPARE_ATTACK = create("entity.conjurer.prepare_attack");
	SoundEvent ENTITY_CONJURER_PREPARE_DISPLACEMENT = create("entity.conjurer.prepare_displacement");

	SoundEvent ENTITY_BOUNCY_BALL_BOUNCE = create("entity.bouncy_ball.bounce");
	SoundEvent ENTITY_BOUNCY_BALL_VANISH = create("entity.bouncy_ball.vanish");

	/*Holder<SoundEvent> RECORD_CONJURED_CHAOS = createForHolder("records.conjured_chaos");*/
	Holder<SoundEvent> RECORD_DELVE_DEEPER = createForHolder("records.delve_deeper");

	public static void init()
	{
	}

	private static SoundEvent create(String name)
	{
		ResourceLocation location = TheConjurerMod.locate(name);
		return Registry.register(BuiltInRegistries.SOUND_EVENT, location, SoundEvent.createVariableRangeEvent(location));
	}

	private static Holder<SoundEvent> createForHolder(String name)
	{
		ResourceLocation location = TheConjurerMod.locate(name);
		return Registry.registerForHolder(BuiltInRegistries.SOUND_EVENT, location, SoundEvent.createVariableRangeEvent(location));
	}

}