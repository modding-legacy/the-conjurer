package com.legacy.conjurer_illager;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URI;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Collectors;

import javax.annotation.Nullable;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import net.minecraft.world.entity.player.Player;

public class MLSupporter
{
	private static final String SUPPORTER_URL = "https://moddinglegacy.com/supporters-changelogs/supporters.txt";
	private final Logger logger;
	private final String modID;
	private Map<UUID, Supporter> supporters = new HashMap<>();

	public MLSupporter(String modID)
	{
		this.logger = LogManager.getLogger("ModdingLegacy/" + modID + "/Supporter");
		this.modID = modID;
	}

	public Map<UUID, Supporter> getSupporters()
	{
		return this.supporters;
	}

	@Nullable
	public Supporter get(Player player)
	{
		return this.supporters.get(player.getUUID());
	}

	public Rank getRank(Player player)
	{
		Supporter supporter = this.get(player);
		return supporter != null ? supporter.rank : Rank.UNKNOWN;
	}

	// HERE Checks the website and updates the internal data accordingly
	public void refresh()
	{
		new GetSupportersThread(this).start();
	}

	@Override
	public String toString()
	{
		StringBuilder builder = new StringBuilder("Modding Legacy supporter list: ").append(this.modID);
		for (Supporter supporter : this.supporters.values())
			builder.append("\n\t").append(supporter.toString());
		return builder.toString();
	}

	public static class Supporter
	{
		public final String name;
		public final UUID uuid;
		public final Rank rank;

		public Supporter(String name, UUID uuid, Rank rank)
		{
			this.name = name;
			this.uuid = uuid;
			this.rank = rank;
		}

		@Override
		public String toString()
		{
			return String.format("[name=%s, UUID=%s, rank=%s]", this.name, this.uuid.toString(), this.rank.name());
		}

		// HERE Read new fields from strings here
		public static Supporter fromString(String string)
		{
			String[] values = string.split(",");
			return new Supporter(values[0], UUID.fromString(values[1]), Rank.fromID(Integer.parseInt(values[2])));
		}
	}

	// HERE Add ranks here
	public static enum Rank
	{
		UNKNOWN(-1, false, false), DEV(0, true, true), SUPPORTER(1, true, true);

		public final int id;
		private final boolean isSupporter;
		private final boolean hasPerks;

		Rank(int id, boolean isSupporter, boolean hasPerks)
		{
			this.id = id;
			this.isSupporter = isSupporter;
			this.hasPerks = hasPerks;
		}

		/**
		 * @return True if developer
		 */
		public boolean isDeveloper()
		{
			return this == DEV;
		}

		/**
		 * @return True if patreon or other
		 */
		public boolean isSupporter()
		{
			return this.isSupporter;
		}

		/**
		 * @return True if supporter perks should be applied
		 */
		public boolean hasPerks()
		{
			return this.hasPerks;
		}

		public static Rank fromID(int id)
		{
			for (Rank rank : Rank.values())
				if (rank.id == id)
					return rank;
			return UNKNOWN;
		}
	}

	private static class GetSupportersThread extends Thread
	{
		private final MLSupporter mlSupporter;

		private GetSupportersThread(MLSupporter mlSupporterInstance)
		{
			super("Modding Legacy/" + mlSupporterInstance.modID + "/Supporters thread");
			this.mlSupporter = mlSupporterInstance;
			this.setDaemon(true);
		}

		@Override
		public void run()
		{
			try
			{
				this.mlSupporter.logger.info("Attempting to load the Modding Legacy supporters list from " + MLSupporter.SUPPORTER_URL);
				List<String> lines = new ArrayList<>();
				URL url = URI.create(MLSupporter.SUPPORTER_URL).toURL();
				HttpURLConnection httpcon = (HttpURLConnection) url.openConnection();
				httpcon.addRequestProperty("User-Agent", "Mozilla/4.0");

				BufferedReader reader = new BufferedReader(new InputStreamReader(httpcon.getInputStream()));

				String line;
				while ((line = reader.readLine()) != null)
					lines.add(line);
				reader.close();

				// Filter out empty lines and lines starting with #. Convert lines to supporters
				// Convert string to Supporter. Null if exception
				// Filter out null values
				// Convert to map of UUID keys and Supporter values
				this.mlSupporter.supporters = lines.stream().filter(s -> !(s.isEmpty() || s.startsWith("#"))).map(l ->
				{
					try
					{
						return Supporter.fromString(l);
					}
					catch (Throwable t)
					{
						this.mlSupporter.logger.error("Failed to load supporter entry for a player!", t);
						return null;
					}
				}).filter(s -> s != null).collect(Collectors.toMap(s -> s.uuid, s -> s));

				this.mlSupporter.logger.info("Successfully loaded the Modding Legacy supporters list.");
			}
			catch (IOException e)
			{
				this.mlSupporter.logger.info("Couldn't load the Modding Legacy supporters list. You may be offline or our website could be having issues. If you are a supporter, some cosmetic features may not work.", e);
			}
			catch (Exception e)
			{
				this.mlSupporter.logger.info("Failed to load the Modding Legacy supporters list. If you are a supporter, some cosmetic features may not work.", e);
			}
		}
	}
}
