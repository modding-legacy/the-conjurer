package com.legacy.conjurer_illager;

import java.util.Optional;

import com.legacy.conjurer_illager.item.ConjurerHatItem;
import com.legacy.conjurer_illager.item.IllagerArmorMaterial;
import com.legacy.conjurer_illager.item.TCRecordItem;
import com.legacy.conjurer_illager.item.ThrowableBallItem;
import com.legacy.conjurer_illager.item.ThrowableCardItem;
import com.legacy.conjurer_illager.registry.TCEntityTypes;
import com.legacy.conjurer_illager.registry.TCJigsawTypes;
import com.legacy.conjurer_illager.registry.TCSounds;
import com.legacy.structure_gel.api.registry.StructureGelRegistries;

import net.minecraft.ChatFormatting;
import net.minecraft.Util;
import net.minecraft.core.Holder;
import net.minecraft.core.registries.Registries;
import net.minecraft.data.worldgen.BootstrapContext;
import net.minecraft.network.chat.Component;
import net.minecraft.resources.ResourceKey;
import net.minecraft.sounds.SoundEvent;
import net.minecraft.world.damagesource.DamageScaling;
import net.minecraft.world.damagesource.DamageType;
import net.minecraft.world.entity.decoration.PaintingVariant;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.JukeboxSong;
import net.minecraft.world.item.Rarity;
import net.minecraft.world.item.SpawnEggItem;
import net.minecraft.world.item.equipment.ArmorType;
import net.minecraft.world.item.equipment.EquipmentAsset;
import net.minecraft.world.item.equipment.EquipmentAssets;
import net.minecraft.world.level.storage.loot.LootTable;
import net.neoforged.bus.api.SubscribeEvent;
import net.neoforged.neoforge.registries.DeferredItem;
import net.neoforged.neoforge.registries.DeferredRegister;
import net.neoforged.neoforge.registries.RegisterEvent;

/**
 * yeah it's a mish-mash of registries, deal with it
 */
public class TCRegistry
{
	@SubscribeEvent
	public static void onRegistry(RegisterEvent event)
	{
		if (event.getRegistryKey().equals(Registries.ENTITY_TYPE))
			TCEntityTypes.init(event);
		else if (event.getRegistryKey().equals(StructureGelRegistries.Keys.JIGSAW_TYPE))
			TCJigsawTypes.init(event);
		else if (event.getRegistryKey().equals(Registries.SOUND_EVENT))
			TCSounds.init();
		else if (event.getRegistryKey().equals(Registries.PARTICLE_TYPE))
			com.legacy.conjurer_illager.registry.TCParticles.init(event);
	}

	public static interface ItemReg
	{
		// really did not feel like doing this the normal way
		DeferredRegister.Items ITEMS = DeferredRegister.createItems(TheConjurerMod.MODID);

		DeferredItem<Item> CONJURER_HAT = ITEMS.registerItem("conjurer_hat", p -> new ConjurerHatItem(IllagerArmorMaterial.CONJURER_HAT, ArmorType.HELMET, p));
		DeferredItem<Item> CONJURER_SPAWN_EGG = ITEMS.registerItem("conjurer_spawn_egg", p -> new SpawnEggItem(TCEntityTypes.CONJURER, p));
		DeferredItem<Item> THROWABLE_BALL = ITEMS.registerItem("throwable_ball", p -> new ThrowableBallItem(p), new Item.Properties().stacksTo(16));
		DeferredItem<Item> THROWING_CARD = ITEMS.registerItem("throwing_card", p -> new ThrowableCardItem(p), new Item.Properties().stacksTo(52));
		DeferredItem<Item> DELVE_DEEPER_RECORD = ITEMS.registerItem("music_disc_delve_deeper", TCRecordItem::new, new Item.Properties().rarity(Rarity.RARE).stacksTo(1).jukeboxPlayable(JukeboxReg.DELVE_DEEPER));
	}

	public static interface EquipmentAssetReg
	{
		ResourceKey<EquipmentAsset> CONJURER_HAT = createId("conjurer_hat");

		static ResourceKey<EquipmentAsset> createId(String key)
		{
			return ResourceKey.create(EquipmentAssets.ROOT_ID, TheConjurerMod.locate(key));
		}

		static void bootstrap(BootstrapContext<EquipmentAsset> context)
		{
			context.register(CONJURER_HAT, new EquipmentAsset());
		}
	}

	public static interface JukeboxReg
	{
		ResourceKey<JukeboxSong> DELVE_DEEPER = create("delve_deeper");

		private static ResourceKey<JukeboxSong> create(String name)
		{
			return ResourceKey.create(Registries.JUKEBOX_SONG, TheConjurerMod.locate(name));
		}

		private static void register(BootstrapContext<JukeboxSong> context, ResourceKey<JukeboxSong> key, Holder<SoundEvent> soundEvent, int lengthInSeconds, int comparatorOutput)
		{
			context.register(key, new JukeboxSong(soundEvent, Component.translatable(Util.makeDescriptionId("jukebox_song", key.location())), (float) lengthInSeconds, comparatorOutput));
		}

		static void bootstrap(BootstrapContext<JukeboxSong> context)
		{
			// lucky 7
			register(context, DELVE_DEEPER, TCSounds.RECORD_DELVE_DEEPER, 230, 7);
		}
	}

	public static interface PaintingReg
	{
		public static final ResourceKey<PaintingVariant> THEATER = create("theater");

		public static void bootstrap(BootstrapContext<PaintingVariant> context)
		{
			register(context, THEATER, 2, 2);
		}

		private static void register(BootstrapContext<PaintingVariant> context, ResourceKey<PaintingVariant> key, int width, int height)
		{
			register(context, key, width, height, true);
		}

		private static void register(BootstrapContext<PaintingVariant> context, ResourceKey<PaintingVariant> key, int width, int height, boolean hasAuthor)
		{
			context.register(key, new PaintingVariant(width, height, key.location(), Optional.of(Component.translatable(key.location().toLanguageKey("painting", "title")).withStyle(ChatFormatting.YELLOW)), hasAuthor ? Optional.of(Component.translatable(key.location().toLanguageKey("painting", "author")).withStyle(ChatFormatting.GRAY)) : Optional.empty()));
		}

		private static ResourceKey<PaintingVariant> create(String name)
		{
			return ResourceKey.create(Registries.PAINTING_VARIANT, TheConjurerMod.locate(name));
		}
	}

	public static interface LootTableReg
	{
		ResourceKey<LootTable> DRESSING_ROOM = create("chests/dressing_room");
		ResourceKey<LootTable> HAT_RABBIT = create("entities/hat_rabbit");

		private static ResourceKey<LootTable> create(String name)
		{
			return ResourceKey.create(Registries.LOOT_TABLE, TheConjurerMod.locate(name));
		}
	}
	
	public static interface DamageTypeReg
	{
		ResourceKey<DamageType> PAPERCUT = create("papercut");
		ResourceKey<DamageType> INDIRECT_PAPERCUT = create("indirect_papercut");

		private static ResourceKey<DamageType> create(String name)
		{
			return ResourceKey.create(Registries.DAMAGE_TYPE, TheConjurerMod.locate(name));
		}

		private static void register(BootstrapContext<DamageType> context, ResourceKey<DamageType> key, DamageScaling scaling, float exhaustion)
		{
			context.register(key, new DamageType(key.location().toString(), scaling, exhaustion));
		}

		static void bootstrap(BootstrapContext<DamageType> context)
		{
			register(context, PAPERCUT, DamageScaling.WHEN_CAUSED_BY_LIVING_NON_PLAYER, 0.1F);
			register(context, INDIRECT_PAPERCUT, DamageScaling.WHEN_CAUSED_BY_LIVING_NON_PLAYER, 0.1F);
		}
	}
}