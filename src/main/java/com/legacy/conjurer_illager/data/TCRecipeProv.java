package com.legacy.conjurer_illager.data;

import java.util.concurrent.CompletableFuture;

import com.legacy.conjurer_illager.TCRegistry;
import com.legacy.conjurer_illager.TheConjurerMod;

import net.minecraft.core.HolderGetter;
import net.minecraft.core.HolderLookup;
import net.minecraft.core.registries.Registries;
import net.minecraft.data.PackOutput;
import net.minecraft.data.recipes.RecipeCategory;
import net.minecraft.data.recipes.RecipeOutput;
import net.minecraft.data.recipes.RecipeProvider;
import net.minecraft.data.recipes.ShapedRecipeBuilder;
import net.minecraft.data.recipes.packs.VanillaRecipeProvider;
import net.minecraft.world.flag.FeatureFlagSet;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.Items;

public class TCRecipeProv extends VanillaRecipeProvider
{
	public static class Runner extends RecipeProvider.Runner
	{
		public Runner(PackOutput output, CompletableFuture<HolderLookup.Provider> registries)
		{
			super(output, registries);
		}

		@Override
		protected RecipeProvider createRecipeProvider(HolderLookup.Provider lookup, RecipeOutput output)
		{
			return new TCRecipeProv(lookup, output);
		}

		@Override
		public String getName()
		{
			return TheConjurerMod.MODID + " Recipe Gen";
		}
	}

	private HolderGetter<Item> items;

	private TCRecipeProv(HolderLookup.Provider lookupProvider, RecipeOutput output)
	{
		super(lookupProvider, output);
		this.items = lookupProvider.lookupOrThrow(Registries.ITEM);
	}

	@Override
	protected void buildRecipes()
	{
		ShapedRecipeBuilder.shaped(this.items, RecipeCategory.COMBAT, TCRegistry.ItemReg.THROWING_CARD, 8).define('C', TCRegistry.ItemReg.THROWING_CARD).define('P', Items.PAPER).define('S', Items.INK_SAC).pattern("PSP").pattern("PCP").pattern("PPP").unlockedBy("has_card", has(TCRegistry.ItemReg.THROWING_CARD)).save(this.output);
	}

	@Override
	protected void generateForEnabledBlockFamilies(FeatureFlagSet flags)
	{
	}
}
