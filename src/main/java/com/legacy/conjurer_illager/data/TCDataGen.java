package com.legacy.conjurer_illager.data;

import java.util.Optional;
import java.util.Set;
import java.util.concurrent.CompletableFuture;

import com.legacy.conjurer_illager.TCRegistry;
import com.legacy.conjurer_illager.TheConjurerMod;
import com.legacy.conjurer_illager.registry.TCStructures;
import com.legacy.structure_gel.api.registry.registrar.RegistrarHandler;

import net.minecraft.DetectedVersion;
import net.minecraft.core.HolderLookup.Provider;
import net.minecraft.core.RegistrySetBuilder;
import net.minecraft.core.registries.Registries;
import net.minecraft.data.CachedOutput;
import net.minecraft.data.DataGenerator;
import net.minecraft.data.DataProvider;
import net.minecraft.data.PackOutput;
import net.minecraft.data.metadata.PackMetadataGenerator;
import net.minecraft.network.chat.Component;
import net.minecraft.server.packs.PackType;
import net.minecraft.server.packs.metadata.pack.PackMetadataSection;
import net.minecraft.util.InclusiveRange;
import net.neoforged.bus.api.SubscribeEvent;
import net.neoforged.neoforge.common.data.BlockTagsProvider;
import net.neoforged.neoforge.common.data.DatapackBuiltinEntriesProvider;
import net.neoforged.neoforge.data.event.GatherDataEvent;

public class TCDataGen
{
	// @formatter:off
	private static final RegistrySetBuilder BUILDER = new RegistrySetBuilder()
		.add(Registries.JUKEBOX_SONG, TCRegistry.JukeboxReg::bootstrap)
		.add(Registries.PAINTING_VARIANT, TCRegistry.PaintingReg::bootstrap)
		.add(Registries.DAMAGE_TYPE, TCRegistry.DamageTypeReg::bootstrap);
	// @formatter:on

	@SubscribeEvent
	public static void gatherData(GatherDataEvent.Client event)
	{
		TCStructures.init();

		DataGenerator gen = event.getGenerator();
		PackOutput output = gen.getPackOutput();
		boolean run = true;

		DatapackBuiltinEntriesProvider provider = new DatapackBuiltinEntriesProvider(gen.getPackOutput(), event.getLookupProvider(), RegistrarHandler.injectRegistries(BUILDER), Set.of(TheConjurerMod.MODID));
		var lookup = provider.getRegistryProvider();

		gen.addProvider(run, provider);

		// this is stupid
		BlockTagsProvider prov = new BlockTagsProvider(output, lookup, TheConjurerMod.MODID)
		{
			@Override
			protected void addTags(Provider provider)
			{
			}
		};
		gen.addProvider(run, prov);

		gen.addProvider(run, new TCTagProv.ItemProv(gen, prov.contentsGetter(), lookup));
		gen.addProvider(run, new TCTagProv.StructureProv(gen, lookup));
		gen.addProvider(run, new TCTagProv.EntityProv(gen, lookup));
		gen.addProvider(run, new TCTagProv.PaintingProv(gen, lookup));
		gen.addProvider(run, new TCTagProv.DamageTypeProv(output, lookup));

		gen.addProvider(run, new TCRecipeProv.Runner(output, lookup));

		gen.addProvider(run, new TCAdvancementProv(gen, lookup));
		gen.addProvider(run, new TCLootProv(gen, event.getLookupProvider()));

		gen.addProvider(run, new TCModelProv(output));
		gen.addProvider(run, new TCModelProv.Equipment(output));

		gen.addProvider(run, new TCLangProv(output, lookup));

		gen.addProvider(run, packMcmeta(output, "The Conjurer resources"));

		PackOutput legacyPackOutput = gen.getPackOutput(String.format("assets/%s/legacy_pack", TheConjurerMod.MODID));
		gen.addProvider(run, packMcmeta(legacyPackOutput, "The Conjurer's old texture based on its indev version"));
	}

	private static final DataProvider packMcmeta(PackOutput output, String description)
	{
		int serverVersion = DetectedVersion.BUILT_IN.getPackVersion(PackType.SERVER_DATA);
		return NestedDataProvider.of(new PackMetadataGenerator(output).add(PackMetadataSection.TYPE, new PackMetadataSection(Component.literal(description), serverVersion, Optional.of(new InclusiveRange<>(0, Integer.MAX_VALUE)))), description);
	}

	private record NestedDataProvider<D extends DataProvider>(D provider, String namePrefix) implements DataProvider
	{

		public static <D extends DataProvider> NestedDataProvider<D> of(D provider, String namePrefix)
		{
			return new NestedDataProvider<>(provider, namePrefix);
		}

		@Override
		public CompletableFuture<?> run(CachedOutput cachedOutput)
		{
			return this.provider.run(cachedOutput);
		}

		@Override
		public String getName()
		{
			return this.namePrefix + "/" + this.provider.getName();
		}
	}
}
