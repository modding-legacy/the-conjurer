package com.legacy.conjurer_illager.data;

import com.legacy.conjurer_illager.TheConjurerMod;

import net.minecraft.core.registries.Registries;
import net.minecraft.tags.TagKey;
import net.minecraft.world.level.levelgen.structure.Structure;

public class TCTags
{
	public static interface Structures
	{
		TagKey<Structure> ON_THEATER_MAPS = tag("on_theater_maps");

		private static TagKey<Structure> tag(String key)
		{
			return TagKey.create(Registries.STRUCTURE, TheConjurerMod.locate(key));
		}
	}
}
