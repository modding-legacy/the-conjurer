package com.legacy.conjurer_illager.data;

import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.function.Consumer;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.legacy.conjurer_illager.TCRegistry;
import com.legacy.conjurer_illager.TheConjurerMod;
import com.legacy.conjurer_illager.registry.TCEntityTypes;
import com.legacy.conjurer_illager.registry.TCStructures;

import net.minecraft.advancements.Advancement;
import net.minecraft.advancements.AdvancementHolder;
import net.minecraft.advancements.AdvancementType;
import net.minecraft.advancements.Criterion;
import net.minecraft.advancements.critereon.ChangeDimensionTrigger;
import net.minecraft.advancements.critereon.EntityPredicate;
import net.minecraft.advancements.critereon.EntityTypePredicate;
import net.minecraft.advancements.critereon.InventoryChangeTrigger;
import net.minecraft.advancements.critereon.KilledTrigger;
import net.minecraft.advancements.critereon.LocationPredicate;
import net.minecraft.advancements.critereon.PlayerTrigger;
import net.minecraft.core.HolderGetter;
import net.minecraft.core.HolderLookup;
import net.minecraft.core.HolderLookup.Provider;
import net.minecraft.core.registries.BuiltInRegistries;
import net.minecraft.core.registries.Registries;
import net.minecraft.data.DataGenerator;
import net.minecraft.data.advancements.AdvancementProvider;
import net.minecraft.data.advancements.AdvancementSubProvider;
import net.minecraft.network.chat.Component;
import net.minecraft.resources.ResourceKey;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.Items;
import net.minecraft.world.level.ItemLike;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.levelgen.structure.Structure;

@SuppressWarnings("unused")
public class TCAdvancementProv extends AdvancementProvider
{
	private static final Gson GSON = (new GsonBuilder()).setPrettyPrinting().create();

	public static AdvancementHolder findTheater, killConjurer;

	public TCAdvancementProv(DataGenerator gen, CompletableFuture<HolderLookup.Provider> lookup)
	{
		super(gen.getPackOutput(), lookup, List.of(new Adv()));
	}

	private static class Adv implements AdvancementSubProvider
	{
		@Override
		public void generate(Provider lookup, Consumer<AdvancementHolder> saver)
		{
			var structureReg = lookup.lookupOrThrow(Registries.STRUCTURE);
			var itemReg = lookup.lookupOrThrow(Registries.ITEM);
			var entityReg = lookup.lookupOrThrow(Registries.ENTITY_TYPE);

			AdvancementHolder adventureRoot = this.builder(Items.STONE, "adventure", AdvancementType.TASK, true, true, false).build(ResourceLocation.withDefaultNamespace("adventure/root"));
			findTheater = builder(TCRegistry.ItemReg.CONJURER_HAT.get(), "find_theatre", AdvancementType.TASK, true, true, false).parent(adventureRoot).addCriterion("in_theater", enterStructure(structureReg, TCStructures.THEATER.getStructure().getKey())).save(saver, TheConjurerMod.find("find_theater"));

			killConjurer = builder(TCRegistry.ItemReg.CONJURER_HAT.get(), "kill_conjurer", AdvancementType.CHALLENGE, true, true, false).parent(findTheater).addCriterion("kill_conjurer", killedEntity(entityReg, TCEntityTypes.CONJURER)).save(saver, TheConjurerMod.find("kill_conjurer"));
		}

		private Advancement.Builder enterAnyStructure(Advancement.Builder builder, HolderGetter<Structure> reg, List<ResourceKey<Structure>> structures)
		{
			structures.forEach(structure -> builder.addCriterion("entered_" + structure.location().getPath(), enterStructure(reg, structure)));
			return builder;
		}

		private Criterion<PlayerTrigger.TriggerInstance> enterStructure(HolderGetter<Structure> reg, ResourceKey<Structure> type)
		{
			return PlayerTrigger.TriggerInstance.located(LocationPredicate.Builder.inStructure(reg.getOrThrow(type)));
		}

		private Criterion<KilledTrigger.TriggerInstance> killedEntity(HolderGetter<EntityType<?>> reg, EntityType<?> type)
		{
			return KilledTrigger.TriggerInstance.playerKilledEntity(EntityPredicate.Builder.entity().entityType(EntityTypePredicate.of(reg, type)));
		}

		private Criterion<ChangeDimensionTrigger.TriggerInstance> changeDim(ResourceKey<Level> type)
		{
			return ChangeDimensionTrigger.TriggerInstance.changedDimensionTo(type);
		}

		private Advancement.Builder haveAnyItem(Advancement.Builder builder, List<ItemLike> items)
		{
			items.forEach(item -> builder.addCriterion("has_" + BuiltInRegistries.ITEM.getKey(item.asItem()).getPath(), InventoryChangeTrigger.TriggerInstance.hasItems(item)));
			return builder;
		}

		private Component translate(String key)
		{
			return Component.translatable("advancements." + TheConjurerMod.MODID + "." + key);
		}

		private Advancement.Builder builder(ItemStack displayItem, String name, ResourceLocation background, AdvancementType AdvancementType, boolean showToast, boolean announceToChat, boolean hidden)
		{
			// FIXME: Titles
			return Advancement.Builder.advancement().display(displayItem, translate(name/* + ".title"*/), translate(name + ".desc"), background, AdvancementType, showToast, announceToChat, hidden);
		}

		private Advancement.Builder builder(ItemLike displayItem, String name, ResourceLocation background, AdvancementType AdvancementType, boolean showToast, boolean announceToChat, boolean hidden)
		{
			return Advancement.Builder.advancement().display(displayItem, translate(name/* + ".title"*/), translate(name + ".desc"), background, AdvancementType, showToast, announceToChat, hidden);
		}

		private Advancement.Builder builder(ItemStack displayItem, String name, AdvancementType AdvancementType, boolean showToast, boolean announceToChat, boolean hidden)
		{
			return builder(displayItem, name, (ResourceLocation) null, AdvancementType, showToast, announceToChat, hidden);
		}

		private Advancement.Builder builder(ItemLike displayItem, String name, AdvancementType AdvancementType, boolean showToast, boolean announceToChat, boolean hidden)
		{
			return builder(displayItem, name, (ResourceLocation) null, AdvancementType, showToast, announceToChat, hidden);
		}

		/*private AdvancementHolder builder(ItemLike displayItem, String name, ResourceLocation background, AdvancementType AdvancementType, boolean showToast, boolean announceToChat, boolean hidden)
		{
			return new AdvancementHolder(Glacidus.locate(name), Advancement.Builder.advancement().display(displayItem, translate(name + ".title"), translate(name + ".desc"), background, AdvancementType, showToast, announceToChat, hidden).save(this.saver, Glacidus.find(name)));
		}*/
	}
}
