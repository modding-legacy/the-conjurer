package com.legacy.conjurer_illager.data;

import java.util.List;
import java.util.Set;
import java.util.concurrent.CompletableFuture;
import java.util.function.BiConsumer;
import java.util.stream.Stream;

import com.legacy.conjurer_illager.TCRegistry;
import com.legacy.conjurer_illager.TheConjurerMod;
import com.legacy.conjurer_illager.registry.TCEntityTypes;

import net.minecraft.core.HolderLookup;
import net.minecraft.core.WritableRegistry;
import net.minecraft.core.registries.BuiltInRegistries;
import net.minecraft.data.DataGenerator;
import net.minecraft.data.loot.LootTableProvider;
import net.minecraft.data.loot.LootTableSubProvider;
import net.minecraft.data.loot.packs.VanillaEntityLoot;
import net.minecraft.resources.ResourceKey;
import net.minecraft.util.ProblemReporter;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.item.Items;
import net.minecraft.world.level.storage.loot.LootPool;
import net.minecraft.world.level.storage.loot.LootTable;
import net.minecraft.world.level.storage.loot.ValidationContext;
import net.minecraft.world.level.storage.loot.entries.EmptyLootItem;
import net.minecraft.world.level.storage.loot.entries.LootItem;
import net.minecraft.world.level.storage.loot.functions.EnchantedCountIncreaseFunction;
import net.minecraft.world.level.storage.loot.functions.SetItemCountFunction;
import net.minecraft.world.level.storage.loot.parameters.LootContextParamSets;
import net.minecraft.world.level.storage.loot.predicates.LootItemKilledByPlayerCondition;
import net.minecraft.world.level.storage.loot.predicates.LootItemRandomChanceWithEnchantedBonusCondition;
import net.minecraft.world.level.storage.loot.providers.number.ConstantValue;
import net.minecraft.world.level.storage.loot.providers.number.UniformGenerator;

public class TCLootProv extends LootTableProvider
{
	public TCLootProv(DataGenerator gen, CompletableFuture<HolderLookup.Provider> lookup)
	{
		super(gen.getPackOutput(), Set.of(), List.of(new LootTableProvider.SubProviderEntry(TCEntityLoot::new, LootContextParamSets.ENTITY), new LootTableProvider.SubProviderEntry(TCChestLoot::new, LootContextParamSets.CHEST)), lookup);
	}

	@Override
	protected void validate(WritableRegistry<LootTable> writableregistry, ValidationContext validationcontext, ProblemReporter.Collector problemreporter$collector)
	{
		writableregistry.listElements().forEach(lootTable -> lootTable.value().validate(validationcontext.setContextKeySet(lootTable.value().getParamSet()).enterElement("{" + lootTable.key().location() + "}", lootTable.key())));
	}

	private static class TCEntityLoot extends VanillaEntityLoot
	{
		public TCEntityLoot(HolderLookup.Provider registries)
		{
			super(registries);
		}

		@Override
		public void generate()
		{
			// @formatter:off
			this.add(TCEntityTypes.CONJURER, LootTable.lootTable()
					.withPool(LootPool.lootPool()
							.setRolls(ConstantValue.exactly(1.0F))
							.add(LootItem.lootTableItem(TCRegistry.ItemReg.THROWING_CARD)
									.apply(SetItemCountFunction.setCount(UniformGenerator.between(8.0F, 16.0F)))
									.apply(EnchantedCountIncreaseFunction.lootingMultiplier(this.registries, UniformGenerator.between(4.0F, 8.0F)))))

					.withPool(LootPool.lootPool()
							.setRolls(ConstantValue.exactly(1.0F))
							.add(LootItem.lootTableItem(TCRegistry.ItemReg.THROWABLE_BALL)
									.apply(SetItemCountFunction.setCount(UniformGenerator.between(2.0F, 3.0F)))
									.apply(EnchantedCountIncreaseFunction.lootingMultiplier(this.registries, UniformGenerator.between(1.0F, 2.0F)))))

					.withPool(LootPool.lootPool()
							.setRolls(ConstantValue.exactly(1.0F))
							.add(LootItem.lootTableItem(TCRegistry.ItemReg.CONJURER_HAT)
									.apply(SetItemCountFunction.setCount(ConstantValue.exactly(1))
											.when(LootItemKilledByPlayerCondition.killedByPlayer()))))

					.withPool(LootPool.lootPool()
							.setRolls(ConstantValue.exactly(1.0F))
							.add(LootItem.lootTableItem(TCRegistry.ItemReg.DELVE_DEEPER_RECORD)
									.apply(SetItemCountFunction.setCount(UniformGenerator.between(0, 1))
											.when(LootItemKilledByPlayerCondition.killedByPlayer())
											.when(LootItemRandomChanceWithEnchantedBonusCondition.randomChanceAndLootingBoost(this.registries, 0.2F, 0.05F)))))
					
					.withPool(
		                    LootPool.lootPool()
		                        .setRolls(ConstantValue.exactly(1.0F))
		                        .add(
		                            LootItem.lootTableItem(Items.EMERALD)
		                                .apply(SetItemCountFunction.setCount(UniformGenerator.between(2.0F, 3.0F)))
		                                .apply(EnchantedCountIncreaseFunction.lootingMultiplier(this.registries, UniformGenerator.between(0.0F, 1.0F)))
		                        )
		                        .when(LootItemKilledByPlayerCondition.killedByPlayer())
		                ));
			// @formatter:on

			this.add(TCEntityTypes.BOUNCING_BALL, LootTable.lootTable());
			this.add(TCEntityTypes.THROWING_CARD, LootTable.lootTable());
		}

		@Override
		public void generate(BiConsumer<ResourceKey<LootTable>, LootTable.Builder> cons)
		{
			super.generate(cons);

			cons.accept(TCRegistry.LootTableReg.HAT_RABBIT, LootTable.lootTable());
		}

		@Override
		protected Stream<EntityType<?>> getKnownEntityTypes()
		{
			return BuiltInRegistries.ENTITY_TYPE.stream().filter(e -> BuiltInRegistries.ENTITY_TYPE.getKey(e).getNamespace().contains(TheConjurerMod.MODID));
		}
	}

	public static record TCChestLoot(HolderLookup.Provider registries) implements LootTableSubProvider
	{
		@Override
		public void generate(BiConsumer<ResourceKey<LootTable>, LootTable.Builder> cons)
		{
			// @formatter:off
	        cons.accept(
	            TCRegistry.LootTableReg.DRESSING_ROOM,
	            LootTable.lootTable()
	                .withPool(
	                    LootPool.lootPool()
	                        .setRolls(UniformGenerator.between(6, 8))
	                        .add(LootItem.lootTableItem(Items.PAPER).apply(SetItemCountFunction.setCount(UniformGenerator.between(10, 15))).setWeight(15))
	                        .add(LootItem.lootTableItem(TCRegistry.ItemReg.THROWING_CARD).apply(SetItemCountFunction.setCount(UniformGenerator.between(5, 10))).setWeight(10))
	                        .add(LootItem.lootTableItem(TCRegistry.ItemReg.THROWABLE_BALL).apply(SetItemCountFunction.setCount(UniformGenerator.between(2, 4))).setWeight(5))
	                        .add(LootItem.lootTableItem(Items.INK_SAC).apply(SetItemCountFunction.setCount(UniformGenerator.between(2, 4))).setWeight(7))
	                        .add(EmptyLootItem.emptyItem().setWeight(5))
	                )
	                .withPool(
	                    LootPool.lootPool()
	                        .setRolls(UniformGenerator.between(1, 2))
	                        .add(LootItem.lootTableItem(Items.EMERALD).apply(SetItemCountFunction.setCount(UniformGenerator.between(4, 8))))
	                )
	                .withPool(
		                    LootPool.lootPool()
		                        .setRolls(ConstantValue.exactly(1.0F))
		                        .add(LootItem.lootTableItem(TCRegistry.ItemReg.DELVE_DEEPER_RECORD))
		                )
	        );
			// @formatter:on
		}
	}
}