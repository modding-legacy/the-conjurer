package com.legacy.conjurer_illager.data;

import static com.legacy.conjurer_illager.TCRegistry.ItemReg.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import java.util.function.BiConsumer;

import com.legacy.conjurer_illager.TCRegistry;
import com.legacy.conjurer_illager.TheConjurerMod;

import net.minecraft.client.color.item.Dye;
import net.minecraft.client.data.models.BlockModelGenerators;
import net.minecraft.client.data.models.EquipmentAssetProvider;
import net.minecraft.client.data.models.ItemModelGenerators;
import net.minecraft.client.data.models.ItemModelOutput;
import net.minecraft.client.data.models.ModelProvider;
import net.minecraft.client.data.models.model.ItemModelUtils;
import net.minecraft.client.data.models.model.ModelInstance;
import net.minecraft.client.data.models.model.ModelLocationUtils;
import net.minecraft.client.data.models.model.ModelTemplates;
import net.minecraft.client.data.models.model.TextureMapping;
import net.minecraft.client.renderer.item.ItemModel;
import net.minecraft.client.renderer.item.SelectItemModel;
import net.minecraft.client.renderer.item.properties.select.TrimMaterialProperty;
import net.minecraft.client.resources.model.EquipmentClientInfo;
import net.minecraft.client.resources.model.EquipmentClientInfo.LayerType;
import net.minecraft.core.component.DataComponents;
import net.minecraft.data.CachedOutput;
import net.minecraft.data.DataProvider;
import net.minecraft.data.PackOutput;
import net.minecraft.resources.ResourceKey;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.equipment.EquipmentAsset;
import net.minecraft.world.item.equipment.trim.TrimMaterial;
import net.neoforged.neoforge.registries.DeferredItem;

public class TCModelProv extends ModelProvider
{
	public TCModelProv(PackOutput output)
	{
		super(output, TheConjurerMod.MODID);
	}

	@Override
	public CompletableFuture<?> run(CachedOutput output)
	{
		return super.run(output);
	}

	@Override
	protected void registerModels(BlockModelGenerators blockModels, ItemModelGenerators itemModels)
	{
		var items = new Items(itemModels.itemModelOutput, itemModels.modelOutput);
		items.run();
	}

	public static class Items extends ItemModelGenerators
	{
		public Items(ItemModelOutput output, BiConsumer<ResourceLocation, ModelInstance> models)
		{
			super(output, models);
		}

		@Override
		public void run()
		{
			this.generateConjurerHat();
			this.basicItem(THROWABLE_BALL);
			this.basicItem(THROWING_CARD);
			this.basicItem(DELVE_DEEPER_RECORD);

			this.generateSpawnEgg(CONJURER_SPAWN_EGG.get(), 0x71265b, 0xe79e4a);
		}

		private void basicItem(DeferredItem<Item> item)
		{
			this.generateFlatItem(item.get(), ModelTemplates.FLAT_ITEM);
		}

		private void handheldItem(Item item)
		{
			this.generateFlatItem(item, ModelTemplates.FLAT_HANDHELD_ITEM);
		}

		public void generateConjurerHat()
		{
			Item item = CONJURER_HAT.get();
			ResourceLocation baseTexture = TextureMapping.getItemTexture(item);
			ResourceLocation overlayTexture = TextureMapping.getItemTexture(item, "_overlay");
			ResourceLocation baseModel = ModelTemplates.FLAT_ITEM.create(item, TextureMapping.layer0(baseTexture), this.modelOutput);
			ResourceLocation dyedModelLocation = ModelLocationUtils.getModelLocation(item, "_dyed");
			ModelTemplates.TWO_LAYERED_ITEM.create(dyedModelLocation, TextureMapping.layered(baseTexture, overlayTexture), this.modelOutput);
			this.itemModelOutput.accept(item, ItemModelUtils.conditional(ItemModelUtils.hasComponent(DataComponents.DYED_COLOR), ItemModelUtils.tintedModel(dyedModelLocation, BLANK_LAYER, new Dye(0)), ItemModelUtils.plainModel(baseModel)));
		}
	}

	public static class Equipment extends EquipmentAssetProvider
	{
		private final PackOutput.PathProvider pathProvider;

		public Equipment(PackOutput output)
		{
			super(output);
			this.pathProvider = output.createPathProvider(PackOutput.Target.RESOURCE_PACK, "equipment");
		}

		@Override
		public CompletableFuture<?> run(CachedOutput p_387304_)
		{
			Map<ResourceKey<EquipmentAsset>, EquipmentClientInfo> map = new HashMap<>();
			bootstrap((asset, info) ->
			{
				if (map.putIfAbsent(asset, info) != null)
					throw new IllegalStateException("Tried to register equipment asset twice for id: " + asset);
			});

			return DataProvider.saveAll(p_387304_, EquipmentClientInfo.CODEC, this.pathProvider::json, map);
		}

		static void bootstrap(BiConsumer<ResourceKey<EquipmentAsset>, EquipmentClientInfo> output)
		{
			var builder = EquipmentClientInfo.builder().addMainHumanoidLayer(TheConjurerMod.locate("conjurer_hat"), false);

			var overlayTex = TheConjurerMod.locate("conjurer_hat_overlay");
			
			builder.addLayers(LayerType.HUMANOID, new EquipmentClientInfo.Layer(overlayTex, Optional.of(new EquipmentClientInfo.Dyeable(Optional.of(0xE79E4A))), false));
			output.accept(TCRegistry.EquipmentAssetReg.CONJURER_HAT, builder.build());
		}
	}
}
