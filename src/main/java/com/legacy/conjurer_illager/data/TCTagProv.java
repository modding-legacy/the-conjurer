package com.legacy.conjurer_illager.data;

import static com.legacy.conjurer_illager.TCRegistry.ItemReg.*;

import java.util.concurrent.CompletableFuture;

import com.legacy.conjurer_illager.TCRegistry;
import com.legacy.conjurer_illager.TheConjurerMod;
import com.legacy.conjurer_illager.registry.TCEntityTypes;
import com.legacy.conjurer_illager.registry.TCStructures;
import com.legacy.structure_gel.api.registry.registrar.StructureRegistrar;

import net.minecraft.core.HolderLookup;
import net.minecraft.core.HolderLookup.Provider;
import net.minecraft.core.registries.Registries;
import net.minecraft.data.DataGenerator;
import net.minecraft.data.PackOutput;
import net.minecraft.data.tags.EntityTypeTagsProvider;
import net.minecraft.data.tags.ItemTagsProvider;
import net.minecraft.data.tags.PaintingVariantTagsProvider;
import net.minecraft.data.tags.StructureTagsProvider;
import net.minecraft.data.tags.TagsProvider;
import net.minecraft.resources.ResourceKey;
import net.minecraft.tags.DamageTypeTags;
import net.minecraft.tags.EntityTypeTags;
import net.minecraft.tags.ItemTags;
import net.minecraft.tags.PaintingVariantTags;
import net.minecraft.tags.TagKey;
import net.minecraft.world.damagesource.DamageType;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.levelgen.structure.Structure;
import net.neoforged.neoforge.common.Tags;

public class TCTagProv
{
	public static class ItemProv extends ItemTagsProvider
	{
		public ItemProv(DataGenerator generatorIn, CompletableFuture<TagLookup<Block>> blocktagProvIn, CompletableFuture<Provider> lookup)
		{
			super(generatorIn.getPackOutput(), lookup, blocktagProvIn, TheConjurerMod.MODID);
		}

		@Override
		protected void addTags(Provider provider)
		{
			conjurer();
			vanilla();
			forge();
		}

		private void conjurer()
		{
		}

		private void vanilla()
		{
			this.tag(ItemTags.EQUIPPABLE_ENCHANTABLE).add(CONJURER_HAT.get());
			this.tag(ItemTags.DURABILITY_ENCHANTABLE).add(CONJURER_HAT.get());

			this.tag(ItemTags.DYEABLE).add(CONJURER_HAT.get());
		}

		private void forge()
		{
			this.tag(Tags.Items.MUSIC_DISCS).add(DELVE_DEEPER_RECORD.get());
		}

		@Override
		public String getName()
		{
			return "Conjurer Item Tags";
		}
	}

	public static class EntityProv extends EntityTypeTagsProvider
	{
		public EntityProv(DataGenerator generatorIn, CompletableFuture<Provider> lookup)
		{
			super(generatorIn.getPackOutput(), lookup, TheConjurerMod.MODID);
		}

		@Override
		protected void addTags(Provider provider)
		{
			conjurer();
			vanilla();
			forge();
		}

		private void conjurer()
		{
		}

		private void vanilla()
		{
			this.tag(EntityTypeTags.ILLAGER).add(TCEntityTypes.CONJURER);
		}

		private void forge()
		{
		}

		@Override
		public String getName()
		{
			return "Conjurer Entity Type Tags";
		}
	}

	public static class StructureProv extends StructureTagsProvider
	{
		public StructureProv(DataGenerator generatorIn, CompletableFuture<Provider> lookup)
		{
			super(generatorIn.getPackOutput(), lookup, TheConjurerMod.MODID);
		}

		@Override
		protected void addTags(Provider prov)
		{
			conjurer();
			vanilla();
			forge();
		}

		void conjurer()
		{
			this.allConfigured(TCTags.Structures.ON_THEATER_MAPS, TCStructures.THEATER);
		}

		void vanilla()
		{
		}

		void forge()
		{
		}

		@SuppressWarnings("unchecked")
		private void allConfigured(TagKey<Structure> tagKey, StructureRegistrar<?>... registrars)
		{
			var appender = this.tag(tagKey);

			for (var entry : registrars)
				entry.getStructures().values().forEach(r -> appender.add((ResourceKey<Structure>) r.getKey()));
		}

		@Override
		public String getName()
		{
			return "Conjurer Structure Tags";
		}
	}

	public static class PaintingProv extends PaintingVariantTagsProvider
	{
		public PaintingProv(DataGenerator generatorIn, CompletableFuture<Provider> lookup)
		{
			super(generatorIn.getPackOutput(), lookup, TheConjurerMod.MODID);
		}

		@Override
		protected void addTags(Provider prov)
		{
			conjurer();
			vanilla();
			forge();
		}

		void conjurer()
		{
		}

		void vanilla()
		{
			this.tag(PaintingVariantTags.PLACEABLE).add(TCRegistry.PaintingReg.THEATER);
		}

		void forge()
		{
		}

		@Override
		public String getName()
		{
			return "Conjurer Painting Variant Tags";
		}
	}

	public static class DamageTypeProv extends TagsProvider<DamageType>
	{
		public DamageTypeProv(PackOutput packOutput, CompletableFuture<HolderLookup.Provider> lookup)
		{
			super(packOutput, Registries.DAMAGE_TYPE, lookup, TheConjurerMod.MODID);
		}

		@Override
		protected void addTags(HolderLookup.Provider lookup)
		{
			ResourceKey<DamageType> papercut = TCRegistry.DamageTypeReg.PAPERCUT,
					indirectPapercut = TCRegistry.DamageTypeReg.INDIRECT_PAPERCUT;

			this.tag(DamageTypeTags.IS_PROJECTILE).add(papercut, indirectPapercut);
			this.tag(DamageTypeTags.NO_KNOCKBACK).add(papercut, indirectPapercut);
			this.tag(DamageTypeTags.BYPASSES_COOLDOWN).add(papercut, indirectPapercut);
		}
	}
}
