package com.legacy.conjurer_illager.item;

import java.util.EnumMap;

import com.legacy.conjurer_illager.TCRegistry;

import net.minecraft.Util;
import net.minecraft.sounds.SoundEvents;
import net.minecraft.tags.ItemTags;
import net.minecraft.world.item.equipment.ArmorMaterial;
import net.minecraft.world.item.equipment.ArmorType;

public interface IllagerArmorMaterial
{

	ArmorMaterial CONJURER_HAT = new ArmorMaterial(5, Util.make(new EnumMap<>(ArmorType.class), values ->
	{
		values.put(ArmorType.BOOTS, 1);
		values.put(ArmorType.LEGGINGS, 2);
		values.put(ArmorType.CHESTPLATE, 3);
		values.put(ArmorType.HELMET, 1);
		values.put(ArmorType.BODY, 3);
	}), 15, SoundEvents.ARMOR_EQUIP_LEATHER, 0.0F, 0.0F, ItemTags.REPAIRS_LEATHER_ARMOR, TCRegistry.EquipmentAssetReg.CONJURER_HAT);
}