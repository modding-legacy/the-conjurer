package com.legacy.conjurer_illager.item;

import com.legacy.conjurer_illager.entity.BouncingBallEntity;

import net.minecraft.server.level.ServerLevel;
import net.minecraft.sounds.SoundEvents;
import net.minecraft.sounds.SoundSource;
import net.minecraft.stats.Stats;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.entity.projectile.Projectile;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.Level;

public class ThrowableBallItem extends Item
{
	public ThrowableBallItem(Item.Properties builder)
	{
		super(builder.overrideDescription("entity.conjurer_illager.bouncing_ball"));
	}

	@Override
	public InteractionResult use(Level level, Player player, InteractionHand hand)
	{
		ItemStack itemstack = player.getItemInHand(hand);
		level.playSound(null, player.getX(), player.getY(), player.getZ(), SoundEvents.SNOWBALL_THROW, SoundSource.NEUTRAL, 0.5F, 0.4F / (level.random.nextFloat() * 0.4F + 0.8F));

		if (level instanceof ServerLevel serverlevel)
		{
			var ball = Projectile.spawnProjectileFromRotation(BouncingBallEntity::new, serverlevel, itemstack, player, 1.0F, 1F, 3F);
			ball.setBallType(player.getRandom().nextInt(5));
		}

		player.awardStat(Stats.ITEM_USED.get(this));
		itemstack.consume(1, player);

		return InteractionResult.SUCCESS;
	}
}