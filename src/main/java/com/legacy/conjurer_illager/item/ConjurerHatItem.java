package com.legacy.conjurer_illager.item;

import java.util.List;

import com.legacy.conjurer_illager.TheConjurerMod;

import net.minecraft.ChatFormatting;
import net.minecraft.network.chat.Component;
import net.minecraft.world.item.ArmorItem;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.TooltipFlag;
import net.minecraft.world.item.equipment.ArmorMaterial;
import net.minecraft.world.item.equipment.ArmorType;
import net.neoforged.api.distmarker.Dist;
import net.neoforged.api.distmarker.OnlyIn;

public class ConjurerHatItem extends ArmorItem
{
	public static final String ABILITY_KEY = String.format("gui.%s.item.conjurer_hat.desc", TheConjurerMod.MODID);

	public ConjurerHatItem(ArmorMaterial materialIn, ArmorType slot, Properties builder)
	{
		super(materialIn, slot, builder);

	}

	@Override
	@OnlyIn(Dist.CLIENT)
	public void appendHoverText(ItemStack stack, Item.TooltipContext context, List<Component> tooltipComponents, TooltipFlag tooltipFlag)
	{
		tooltipComponents.add(Component.translatable(ABILITY_KEY, "30%").withStyle(ChatFormatting.GOLD));
	}
}