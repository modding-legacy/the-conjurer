package com.legacy.conjurer_illager.item;

import com.legacy.conjurer_illager.entity.ThrowingCardEntity;

import net.minecraft.server.level.ServerLevel;
import net.minecraft.sounds.SoundEvents;
import net.minecraft.sounds.SoundSource;
import net.minecraft.stats.Stats;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.entity.projectile.Projectile;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.Level;

public class ThrowableCardItem extends Item
{
	public ThrowableCardItem(Item.Properties builder)
	{
		super(builder.overrideDescription("entity.conjurer_illager.throwing_card"));
	}

	@Override
	public InteractionResult use(Level level, Player player, InteractionHand hand)
	{
		ItemStack itemstack = player.getItemInHand(hand);
		level.playSound(null, player.getX(), player.getY(), player.getZ(), SoundEvents.BOOK_PAGE_TURN, SoundSource.NEUTRAL, 1, 1);

		if (level instanceof ServerLevel serverlevel)
		{
			var card = Projectile.spawnProjectileFromRotation(ThrowingCardEntity::new, serverlevel, itemstack, player, 1.0F, 1F, 3F);
			card.setCardType(player.getRandom().nextInt(2));
		}

		player.awardStat(Stats.ITEM_USED.get(this));
		itemstack.consume(1, player);

		return InteractionResult.SUCCESS;
	}
}