package com.legacy.conjurer_illager.item;

import com.legacy.conjurer_illager.TheConjurerMod;

import net.minecraft.world.item.Item;

public class TCRecordItem extends Item
{
	public static final String NAME_KEY = String.format("item.%s.music_disc", TheConjurerMod.MODID);

	public TCRecordItem(Properties properties)
	{
		super(properties.overrideDescription(NAME_KEY));
	}

}
