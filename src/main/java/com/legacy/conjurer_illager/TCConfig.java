package com.legacy.conjurer_illager;

import org.apache.commons.lang3.tuple.Pair;

import net.neoforged.neoforge.common.ModConfigSpec;

public class TCConfig
{
	public static final Common COMMON;
	public static final ModConfigSpec COMMON_SPEC;
	static
	{
		Pair<Common, ModConfigSpec> specPair = new ModConfigSpec.Builder().configure(Common::new);
		COMMON_SPEC = specPair.getRight();
		COMMON = specPair.getLeft();
	}

	public static class Common
	{
		/*public final StructureConfig theaterStructureConfig;*/

		public Common(ModConfigSpec.Builder builder)
		{
			/*this.theaterStructureConfig = StructureConfig.builder(builder, "Theater").placement(40, 35, 50).pushStructure().biomes("minecraft:dark_forest").popStructure().build();*/
		}
	}
}
