package com.legacy.conjurer_illager.world;

import com.legacy.conjurer_illager.TheConjurerMod;
import com.legacy.conjurer_illager.registry.IllagerProcessors;
import com.legacy.structure_gel.api.structure.jigsaw.JigsawPoolBuilder;
import com.legacy.structure_gel.api.structure.jigsaw.JigsawRegistryHelper;

import net.minecraft.core.registries.Registries;
import net.minecraft.data.worldgen.BootstrapContext;
import net.minecraft.resources.ResourceKey;
import net.minecraft.world.level.levelgen.structure.pools.StructureTemplatePool;

public class TheaterPools
{
	/*public static final Holder<StructureTemplatePool> ROOT;
	
	public static void init()
	{
	}
	
	static
	{
		JigsawRegistryHelper registry = new JigsawRegistryHelper(TheConjurerMod.MODID, "theatre/");
	
		ROOT = registry.register("root", registry.builder().names("root").build());
		registry.register("lobby", registry.builder().names("lobby_1", "lobby_2").maintainWater(false).build());
		registry.register("stage", registry.builder().names("stage_1", "stage_2").maintainWater(false).processors(IllagerProcessors.COBBLE_WALLS_TO_GRASS).build());
		registry.register("offshoot", registry.builder().names("offshoot/closet", "offshoot/dressing_room").maintainWater(false).processors(IllagerProcessors.COBBLE_WALLS_TO_GRASS).build());
		registry.register("scene", registry.builder().names("scene/joker", "scene/titanic", "scene/superman", "scene/indiana_jones", "scene/wizard_of_oz").maintainWater(false).build());
	}*/

	public static final ResourceKey<StructureTemplatePool> ROOT = ResourceKey.create(Registries.TEMPLATE_POOL, TheConjurerMod.locate("theatre/root"));

	public static void bootstrap(BootstrapContext<StructureTemplatePool> bootstrap)
	{
		JigsawRegistryHelper registry = new JigsawRegistryHelper(TheConjurerMod.MODID, "theatre/", bootstrap);
		JigsawPoolBuilder builder = registry.poolBuilder();

		registry.registerBuilder().pools(builder.clone().names("root")).register("root");
		registry.register("lobby", builder.names("lobby_1", "lobby_2").maintainWater(false).build());
		registry.register("stage", builder.names("stage_1", "stage_2").maintainWater(false).processors(IllagerProcessors.COBBLE_WALLS_TO_GRASS.getKey()).build());
		registry.register("offshoot", builder.names("offshoot/closet", "offshoot/dressing_room").maintainWater(false).processors(IllagerProcessors.COBBLE_WALLS_TO_GRASS.getKey()).build());
		registry.register("scene", builder.names("scene/joker", "scene/titanic", "scene/superman", "scene/indiana_jones", "scene/wizard_of_oz").maintainWater(false).build());
	}
}
