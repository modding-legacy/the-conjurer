package com.legacy.conjurer_illager.world;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.legacy.conjurer_illager.MLSupporter;
import com.legacy.conjurer_illager.TheConjurerMod;
import com.legacy.conjurer_illager.entity.ConjurerEntity;
import com.legacy.conjurer_illager.registry.TCEntityTypes;
import com.legacy.conjurer_illager.registry.TCJigsawTypes;
import com.legacy.conjurer_illager.registry.TCStructures;
import com.legacy.structure_gel.api.structure.jigsaw.ExtendedJigsawStructurePiece;
import com.legacy.structure_gel.api.structure.jigsaw.IPieceFactory;
import com.legacy.structure_gel.api.structure.jigsaw.JigsawCapability;
import com.legacy.structure_gel.api.structure.jigsaw.JigsawCapabilityType;
import com.mojang.serialization.MapCodec;

import net.minecraft.core.BlockPos;
import net.minecraft.core.component.DataComponents;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.chat.Component;
import net.minecraft.server.network.Filterable;
import net.minecraft.server.network.FilteredText;
import net.minecraft.util.RandomSource;
import net.minecraft.world.entity.EntitySpawnReason;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.entity.Mob;
import net.minecraft.world.entity.ai.goal.LookAtPlayerGoal;
import net.minecraft.world.entity.animal.TropicalFish;
import net.minecraft.world.entity.monster.Pillager;
import net.minecraft.world.entity.npc.Villager;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.Items;
import net.minecraft.world.item.component.WrittenBookContent;
import net.minecraft.world.level.ServerLevelAccessor;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.entity.LecternBlockEntity;
import net.minecraft.world.level.levelgen.structure.BoundingBox;
import net.minecraft.world.level.levelgen.structure.pieces.StructurePieceSerializationContext;
import net.minecraft.world.level.levelgen.structure.pieces.StructurePieceType;

public class TheaterStructure
{
	public static class Capability implements JigsawCapability
	{
		public static final Capability INSTANCE = new Capability();
		public static final MapCodec<Capability> CODEC = MapCodec.unit(INSTANCE);

		@Override
		public JigsawCapabilityType<?> getType()
		{
			return TCJigsawTypes.THEATER;
		}

		@Override
		public IPieceFactory getPieceFactory()
		{
			return Piece::new;
		}
	}

	public static class Piece extends ExtendedJigsawStructurePiece
	{
		public Piece(IPieceFactory.Context context)
		{
			super(context);
		}

		public Piece(StructurePieceSerializationContext context, CompoundTag nbt)
		{
			super(context, nbt);
		}

		@SuppressWarnings("deprecation")
		@Override
		public void handleDataMarker(String key, BlockPos pos, ServerLevelAccessor worldIn, RandomSource rand, BoundingBox bounds)
		{
			if (key.equals("conjurer"))
			{
				setAir(worldIn, pos);
				ConjurerEntity entity = createEntity(TCEntityTypes.CONJURER, worldIn, pos, this.rotation);
				entity.finalizeSpawn(worldIn, worldIn.getCurrentDifficultyAt(pos), EntitySpawnReason.STRUCTURE, null);
				entity.naturalSpawn = true;
				entity.setPersistenceRequired();
				worldIn.addFreshEntity(entity);
			}
			else if (key.equals("admissioner"))
			{
				setAir(worldIn, pos);
				Pillager entity = createEntity(EntityType.PILLAGER, worldIn, pos, this.rotation);
				entity.finalizeSpawn(worldIn, worldIn.getCurrentDifficultyAt(pos), EntitySpawnReason.STRUCTURE, null);
				entity.setPersistenceRequired();
				worldIn.addFreshEntity(entity);

				entity.restrictTo(pos, 1);
			}
			else if (key.equals("villager"))
			{
				setAir(worldIn, pos);
				Villager entity = createEntity(EntityType.VILLAGER, worldIn, pos, this.rotation);
				entity.finalizeSpawn(worldIn, worldIn.getCurrentDifficultyAt(pos), EntitySpawnReason.STRUCTURE, null);
				worldIn.addFreshEntity(entity);

				entity.goalSelector.addGoal(9, new LookAtPlayerGoal(entity, ConjurerEntity.class, 15.0F, 2.0F));
			}
			else if (key.equals("audience"))
			{
				setAir(worldIn, pos);
				Mob entity = createEntity(EntityType.PILLAGER, worldIn, pos, this.rotation);

				if (rand.nextFloat() < 0.05F)
					entity = createEntity(EntityType.ILLUSIONER, worldIn, pos, this.rotation);
				else if (rand.nextFloat() < 0.40F)
					entity = createEntity(EntityType.VINDICATOR, worldIn, pos, this.rotation);
				else if (rand.nextFloat() < 0.20F)
					entity = createEntity(EntityType.WITCH, worldIn, pos, this.rotation);

				entity.finalizeSpawn(worldIn, worldIn.getCurrentDifficultyAt(pos), EntitySpawnReason.STRUCTURE, null);
				entity.setPersistenceRequired();
				worldIn.addFreshEntity(entity);

				entity.restrictTo(pos, 1);
				entity.goalSelector.addGoal(9, new LookAtPlayerGoal(entity, ConjurerEntity.class, 15.0F, 2.0F));
			}
			else if (key.contains("lectern"))
			{
				setAir(worldIn, pos);

				ItemStack bookStack = new ItemStack(Items.WRITTEN_BOOK);
				List<FilteredText> pages = new ArrayList<>();

				String pageText = "VIP Admission List\n -=-=-=-=-=-=-=-=-\n";
				String[] names = TheConjurerMod.SUPPORTERS.getSupporters().values().stream().filter(mls -> mls.rank == MLSupporter.Rank.DEV || mls.rank == MLSupporter.Rank.SUPPORTER).map(mls -> mls.name).toArray(String[]::new);

				if (names.length > 0)
				{
					for (int i = 0; i < 12; i++)
					{
						String name = names[rand.nextInt(names.length)];
						pageText = pageText + name + "\n";
						names = Arrays.asList(names).stream().filter(s -> s != name).toArray(String[]::new);
					}
					pages.add(FilteredText.passThrough(pageText));

					List<Filterable<Component>> list = pages.stream().map(text -> Filterable.from(text).<Component>map(Component::literal)).toList();
					bookStack.set(DataComponents.WRITTEN_BOOK_CONTENT, new WrittenBookContent(Filterable.passThrough("Admission List"), "The Conjurer", 0, list, true));

					LecternBlockEntity lectern = (LecternBlockEntity) worldIn.getBlockEntity(pos.below());
					lectern.setBook(bookStack);
				}
			}
			else if (key.equals("fish"))
			{
				worldIn.setBlock(pos, Blocks.SANDSTONE.defaultBlockState(), 3);
				TropicalFish entity = createEntity(EntityType.TROPICAL_FISH, worldIn, pos.above(rand.nextInt(3) + 1), this.rotation);
				entity.finalizeSpawn(worldIn, worldIn.getCurrentDifficultyAt(pos), EntitySpawnReason.STRUCTURE, null);
				entity.setPersistenceRequired();
				worldIn.addFreshEntity(entity);
			}
		}

		@Override
		public StructurePieceType getType()
		{
			return TCStructures.THEATER.getPieceType().get();
		}
	}
}
