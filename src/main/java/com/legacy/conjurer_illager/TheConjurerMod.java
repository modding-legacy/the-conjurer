package com.legacy.conjurer_illager;

import com.legacy.conjurer_illager.client.TCClientEvents;
import com.legacy.conjurer_illager.client.render.TCEntityRendering;
import com.legacy.conjurer_illager.data.TCDataGen;
import com.legacy.conjurer_illager.registry.IllagerProcessors;
import com.legacy.conjurer_illager.registry.TCEntityTypes;
import com.legacy.conjurer_illager.registry.TCStructures;
import com.legacy.structure_gel.api.registry.registrar.RegistrarHandler;

import net.minecraft.resources.ResourceLocation;
import net.neoforged.api.distmarker.Dist;
import net.neoforged.bus.api.EventPriority;
import net.neoforged.bus.api.IEventBus;
import net.neoforged.fml.common.Mod;
import net.neoforged.fml.event.lifecycle.FMLCommonSetupEvent;
import net.neoforged.fml.loading.FMLEnvironment;
import net.neoforged.neoforge.common.NeoForge;

@Mod(TheConjurerMod.MODID)
public class TheConjurerMod
{
	public static final String NAME = "The Conjurer";
	public static final String MODID = "conjurer_illager";
	public static final MLSupporter SUPPORTERS = new MLSupporter(MODID);

	public TheConjurerMod(IEventBus modBus)
	{
		/*ModLoadingContext.get().registerConfig(ModConfig.Type.COMMON, IllagerConfig.COMMON_SPEC);*/

		IEventBus forge = NeoForge.EVENT_BUS;

		TCRegistry.ItemReg.ITEMS.register(modBus);

		if (FMLEnvironment.dist == Dist.CLIENT)
		{
			TCEntityRendering.init(modBus);
			modBus.addListener(TCClientEvents::registerClientExtensions);
			/*modBus.addListener((RegisterColorHandlersEvent.Item event) -> event.register((stack, layer) -> layer == 0 ? ((DyeableLeatherItem) stack.getItem()).getColor(stack) : -1, TCRegistry.CONJURER_HAT.get()));*/
			modBus.addListener(com.legacy.conjurer_illager.registry.TCParticles.Register::registerParticleFactories);
		}

		TCStructures.init();
		RegistrarHandler.registerHandlers(MODID, modBus, IllagerProcessors.HANDLER);

		modBus.addListener(TheConjurerMod::commonInit);
		modBus.addListener(TCEntityTypes::onAttributesRegistered);
		modBus.addListener(EventPriority.LOWEST, TCEntityTypes::registerPlacements);
		modBus.register(TCRegistry.class);
		forge.register(TCEvents.class);

		modBus.register(TCDataGen.class);
	}

	private static void commonInit(FMLCommonSetupEvent event)
	{
		SUPPORTERS.refresh();
		
		/*event.enqueueWork(() ->
		{
			BuiltInRegistries.ITEM.addAlias(locate("throwable_ball"), locate("bouncy_ball"));
		});*/
	}

	public static ResourceLocation locate(String name)
	{
		return ResourceLocation.fromNamespaceAndPath(MODID, name);
	}

	public static String find(String name)
	{
		return locate(name).toString();
	}
}
