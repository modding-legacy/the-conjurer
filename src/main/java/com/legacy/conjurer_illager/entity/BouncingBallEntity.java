package com.legacy.conjurer_illager.entity;

import com.legacy.conjurer_illager.TCRegistry;
import com.legacy.conjurer_illager.registry.TCEntityTypes;
import com.legacy.conjurer_illager.registry.TCSounds;

import net.minecraft.core.Direction;
import net.minecraft.core.particles.ParticleTypes;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.syncher.EntityDataAccessor;
import net.minecraft.network.syncher.EntityDataSerializers;
import net.minecraft.network.syncher.SynchedEntityData;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.sounds.SoundEvents;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.entity.animal.Rabbit;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.entity.projectile.ThrowableItemProjectile;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.phys.BlockHitResult;
import net.minecraft.world.phys.EntityHitResult;
import net.minecraft.world.phys.HitResult;
import net.minecraft.world.phys.Vec3;

public class BouncingBallEntity extends ThrowableItemProjectile
{
	private static final EntityDataAccessor<Integer> BALL_TYPE = SynchedEntityData.<Integer>defineId(BouncingBallEntity.class, EntityDataSerializers.INT);
	private static final EntityDataAccessor<Integer> BOUNCES = SynchedEntityData.<Integer>defineId(BouncingBallEntity.class, EntityDataSerializers.INT);

	public BouncingBallEntity(EntityType<? extends BouncingBallEntity> type, Level world)
	{
		super(type, world);
	}

	public BouncingBallEntity(Level level, LivingEntity owner, ItemStack item)
	{
		super(TCEntityTypes.BOUNCING_BALL, owner, level, item);
	}

	public BouncingBallEntity(EntityType<? extends BouncingBallEntity> type, LivingEntity thrower, Level level, ItemStack item)
	{
		super(type, thrower, level, item);
	}

	@Override
	protected Item getDefaultItem()
	{
		return TCRegistry.ItemReg.THROWABLE_BALL.get();
	}

	@Override
	public void addAdditionalSaveData(CompoundTag compound)
	{
		super.addAdditionalSaveData(compound);
		compound.putInt("ballType", this.getBallType());
		compound.putInt("totalBounces", this.getTotalBounces());
	}

	@Override
	public void readAdditionalSaveData(CompoundTag compound)
	{
		super.readAdditionalSaveData(compound);
		this.setBallType(compound.getInt("ballType"));
		this.setTotalBounces(compound.getInt("totalBounces"));
	}

	@Override
	protected void onHit(HitResult result)
	{
		HitResult.Type raytraceresult$type = result.getType();
		if (raytraceresult$type == HitResult.Type.BLOCK)
		{
			BlockHitResult traceResult = (BlockHitResult) result;
			BlockState blockstate = this.level().getBlockState(traceResult.getBlockPos());
			if (!blockstate.getCollisionShape(this.level(), traceResult.getBlockPos()).isEmpty())
			{
				Direction face = traceResult.getDirection();
				blockstate.onProjectileHit(this.level(), blockstate, traceResult, this);

				Vec3 motion = this.getDeltaMovement();

				double motionX = motion.x();
				double motionY = motion.y();
				double motionZ = motion.z();

				if (face == Direction.EAST)
					motionX = -motionX;
				else if (face == Direction.SOUTH)
					motionZ = -motionZ;
				else if (face == Direction.WEST)
					motionX = -motionX;
				else if (face == Direction.NORTH)
					motionZ = -motionZ;
				else if (face == Direction.UP)
					motionY = -motionY;
				else if (face == Direction.DOWN)
					motionY = -motionY;

				this.setDeltaMovement(motionX, motionY, motionZ);

				boolean player = this.getOwner() instanceof Player;

				if (this.level() instanceof ServerLevel level && (this.tickCount > 200 || this.getTotalBounces() > (!player ? 20 : 5)))
				{
					this.playSound(TCSounds.ENTITY_BOUNCY_BALL_VANISH, 1.0F, 2.0F);

					if (player)
						this.spawnAtLocation(level, this.getItem());

					this.discard();
				}
				else
				{
					this.playSound(TCSounds.ENTITY_BOUNCY_BALL_BOUNCE, 1.0F, (this.random.nextFloat() - this.random.nextFloat()) * 0.2F + 1.0F);
					this.setTotalBounces(this.getTotalBounces() + 1);
				}
			}
		}

		if (raytraceresult$type == HitResult.Type.ENTITY)
		{
			Entity entity = ((EntityHitResult) result).getEntity();

			// getOwner = getThrower
			if (this.getOwner() != null && !entity.isAlliedTo(this.getOwner()) && !(entity instanceof Rabbit))
			{
				boolean player = this.getOwner() instanceof Player;

				int i = !player ? 5 + this.level().getDifficulty().getId() : 6;

				if (!entity.isInvulnerable() && this.level() instanceof ServerLevel level)
				{
					var source = this.damageSources().indirectMagic(this, this.getOwner());

					entity.hurtServer(level, source, (float) i);
					this.playSound(TCSounds.ENTITY_BOUNCY_BALL_VANISH, 1.0F, 2.0F);

					if (!player)
						this.playSound(SoundEvents.CHORUS_FRUIT_TELEPORT, 0.5F, 2.0F);
					else
						this.spawnAtLocation(level, this.getItem());

					this.discard();
				}
			}
		}
	}

	@Override
	public void tick()
	{
		super.tick();

		if (this.level() instanceof ServerLevel level)
		{
			if (this.isNoGravity())
				this.setDeltaMovement(this.getDeltaMovement().scale(1.01D));

			if (this.tickCount > 360 || this.tickCount > 150 && level.canSeeSky(this.blockPosition()) || this.getOwner() != null && !this.getOwner().isAlive())
			{
				if (!(this.getOwner() instanceof Player))
					this.playSound(SoundEvents.CHORUS_FRUIT_TELEPORT, 0.5F, 2.0F);
				else
					this.spawnAtLocation(level, this.getItem());

				this.playSound(TCSounds.ENTITY_BOUNCY_BALL_VANISH, 1.0F, 2.0F);
				this.discard();
			}
		}

		for (int i = 0; i < 2; ++i)
		{
			this.level().addParticle(ParticleTypes.WITCH, this.getRandomX(0.5D), this.getRandomY(), this.getRandomZ(0.5D), this.random.nextGaussian() * 0.1D, 0.0D, this.random.nextGaussian() * 0.1D);
		}
	}

	@Override
	protected void defineSynchedData(SynchedEntityData.Builder builder)
	{
		super.defineSynchedData(builder);
		builder.define(BALL_TYPE, 0);
		builder.define(BOUNCES, 0);
	}

	public int getBallType()
	{
		return this.entityData.get(BALL_TYPE);
	}

	public void setBallType(int type)
	{
		this.entityData.set(BALL_TYPE, type);
	}

	public int getTotalBounces()
	{
		return this.entityData.get(BOUNCES);
	}

	public void setTotalBounces(int bounces)
	{
		this.entityData.set(BOUNCES, bounces);
	}
}
