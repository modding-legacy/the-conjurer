package com.legacy.conjurer_illager.entity;

import javax.annotation.Nullable;

import com.legacy.conjurer_illager.TCRegistry;
import com.legacy.conjurer_illager.registry.TCEntityTypes;

import net.minecraft.core.particles.ParticleTypes;
import net.minecraft.core.registries.Registries;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.syncher.EntityDataAccessor;
import net.minecraft.network.syncher.EntityDataSerializers;
import net.minecraft.network.syncher.SynchedEntityData;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.world.damagesource.DamageSource;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.entity.animal.Rabbit;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.entity.projectile.ThrowableItemProjectile;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.phys.BlockHitResult;
import net.minecraft.world.phys.EntityHitResult;
import net.minecraft.world.phys.HitResult;

public class ThrowingCardEntity extends ThrowableItemProjectile
{
	private static final EntityDataAccessor<Integer> CARD_TYPE = SynchedEntityData.<Integer>defineId(ThrowingCardEntity.class, EntityDataSerializers.INT);
	/*private static final DamageSource CARD_SOURCE = new DamageSource(TheConjurerMod.find("papercut")).setProjectile();*/

	public ThrowingCardEntity(EntityType<? extends ThrowingCardEntity> type, Level world)
	{
		super(type, world);
	}

	public ThrowingCardEntity(Level level, LivingEntity owner, ItemStack item)
	{
		super(TCEntityTypes.THROWING_CARD, owner, level, item);
	}

	public ThrowingCardEntity(EntityType<? extends ThrowingCardEntity> type, LivingEntity thrower, Level level, ItemStack item)
	{
		super(type, thrower, level, item);
	}

	@Override
	protected Item getDefaultItem()
	{
		return TCRegistry.ItemReg.THROWING_CARD.get();
	}

	@Override
	protected void onHit(HitResult result)
	{
		HitResult.Type raytraceresult$type = result.getType();

		if (this.level() instanceof ServerLevel level)
		{
			if (raytraceresult$type == HitResult.Type.BLOCK)
			{
				BlockHitResult traceResult = (BlockHitResult) result;
				BlockState blockstate = this.level().getBlockState(traceResult.getBlockPos());
				blockstate.onProjectileHit(this.level(), blockstate, traceResult, this);

				if (this.getOwner() instanceof Player player && !player.isCreative())
					this.spawnAtLocation(level, this.getItem());

				this.discard();
			}

			if (raytraceresult$type == HitResult.Type.ENTITY)
			{
				Entity entity = ((EntityHitResult) result).getEntity();

				int i = 1;

				if (this.getCardType() == 2)
					i += 1;

				var owner = this.getOwner();
				// getOwner = getThrower
				if (owner != null && !entity.isAlliedTo(owner) && !(entity instanceof Rabbit))
				{
					if (owner instanceof ConjurerEntity)
						i += 1;

					if (!entity.isInvulnerable())
					{
						var source = this.getCardSource(owner, entity);

						entity.hurtServer(level, source, i);
						this.discard();
					}
				}
				else if (owner == null)
				{
					if (!entity.isInvulnerable())
					{
						var source = this.getCardSource(this.getOwner(), entity);

						entity.hurtServer(level, source, i);
						this.discard();
					}
				}
			}
		}
	}

	@Override
	public void tick()
	{
		super.tick();

		if (this.random.nextFloat() < 0.3F)
			this.level().addParticle(this.getCardType() == 2 ? ParticleTypes.ENCHANTED_HIT : ParticleTypes.CRIT, this.getX(), this.getY(), this.getZ(), this.random.nextGaussian() * 0.1D, 0.0D, this.random.nextGaussian() * 0.1D);
	}

	@Override
	public void addAdditionalSaveData(CompoundTag compound)
	{
		super.addAdditionalSaveData(compound);
		compound.putInt("CardType", this.getCardType());
	}

	@Override
	public void readAdditionalSaveData(CompoundTag compound)
	{
		super.readAdditionalSaveData(compound);
		this.setCardType(compound.getInt("CardType"));
	}

	@Override
	protected void defineSynchedData(SynchedEntityData.Builder builder)
	{
		super.defineSynchedData(builder);
		builder.define(CARD_TYPE, 0);
	}

	public int getCardType()
	{
		return this.entityData.get(CARD_TYPE);
	}

	public void setCardType(int type)
	{
		this.entityData.set(CARD_TYPE, type);
	}

	private DamageSource getCardSource(@Nullable Entity owner, @Nullable Entity damaged)
	{
		var reg = this.level().registryAccess().lookupOrThrow(Registries.DAMAGE_TYPE);
		return owner != null ? new DamageSource(reg.getOrThrow(TCRegistry.DamageTypeReg.INDIRECT_PAPERCUT), this, owner) : new DamageSource(reg.getOrThrow(TCRegistry.DamageTypeReg.PAPERCUT));
	}
}
