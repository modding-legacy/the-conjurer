package com.legacy.conjurer_illager.entity;

import java.util.EnumSet;
import java.util.List;

import javax.annotation.Nullable;

import com.legacy.conjurer_illager.TCRegistry;
import com.legacy.conjurer_illager.registry.TCEntityTypes;
import com.legacy.conjurer_illager.registry.TCParticles;
import com.legacy.conjurer_illager.registry.TCSounds;
import com.legacy.structure_gel.api.entity.EntityAccessHelper;

import net.minecraft.core.BlockPos;
import net.minecraft.core.Vec3i;
import net.minecraft.core.particles.ColorParticleOption;
import net.minecraft.core.particles.ParticleOptions;
import net.minecraft.core.particles.ParticleTypes;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.sounds.SoundEvent;
import net.minecraft.sounds.SoundEvents;
import net.minecraft.sounds.SoundSource;
import net.minecraft.tags.DamageTypeTags;
import net.minecraft.util.Mth;
import net.minecraft.util.RandomSource;
import net.minecraft.world.DifficultyInstance;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.damagesource.DamageSource;
import net.minecraft.world.effect.MobEffectInstance;
import net.minecraft.world.effect.MobEffects;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.EntitySpawnReason;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.entity.EquipmentSlot;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.entity.SpawnGroupData;
import net.minecraft.world.entity.ai.attributes.AttributeSupplier;
import net.minecraft.world.entity.ai.attributes.Attributes;
import net.minecraft.world.entity.ai.goal.AvoidEntityGoal;
import net.minecraft.world.entity.ai.goal.Goal;
import net.minecraft.world.entity.ai.goal.LookAtPlayerGoal;
import net.minecraft.world.entity.ai.goal.MeleeAttackGoal;
import net.minecraft.world.entity.ai.goal.WaterAvoidingRandomStrollGoal;
import net.minecraft.world.entity.ai.goal.target.HurtByTargetGoal;
import net.minecraft.world.entity.ai.goal.target.NearestAttackableTargetGoal;
import net.minecraft.world.entity.animal.IronGolem;
import net.minecraft.world.entity.animal.Rabbit;
import net.minecraft.world.entity.monster.AbstractIllager;
import net.minecraft.world.entity.monster.Monster;
import net.minecraft.world.entity.monster.SpellcasterIllager;
import net.minecraft.world.entity.npc.AbstractVillager;
import net.minecraft.world.entity.npc.Villager;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.entity.raid.Raider;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.ServerLevelAccessor;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.phys.AABB;
import net.minecraft.world.phys.Vec3;

public class ConjurerEntity extends SpellcasterIllager
{
	private static final Vec3i BOUNCY_BALL_SPELL_COLOR = new Vec3i(243, 162, 43);
	private static final Vec3i RABBIT_SPELL_COLOR = new Vec3i(255, 208, 208);
	private static final Vec3i DISPLACE_SPELL_COLOR = new Vec3i(165, 96, 201);
	private static final Vec3i DISAPPEAR_SPELL_COLOR = new Vec3i(77, 77, 204);

	public int throwingCardCooldown = 0;
	public boolean naturalSpawn = false;

	public ConjurerEntity(EntityType<? extends ConjurerEntity> type, Level worldIn)
	{
		super(type, worldIn);
		this.xpReward = 15;
	}

	@Override
	protected void registerGoals()
	{
		super.registerGoals();
		this.goalSelector.addGoal(1, new ConjurerEntity.CastingSpellGoal());
		this.goalSelector.addGoal(4, new ConjurerEntity.DisappearSpellGoal());
		this.goalSelector.addGoal(4, new ConjurerEntity.ThrowingCardAttackGoal());
		this.goalSelector.addGoal(5, new ConjurerEntity.BouncyBallSpellGoal());
		this.goalSelector.addGoal(6, new ConjurerEntity.BunnySpellGoal());
		this.goalSelector.addGoal(6, new ConjurerEntity.DisplaceSpellGoal());

		this.goalSelector.addGoal(9, new LookAtPlayerGoal(this, Player.class, 15.0F, 2.0F)
		{
			@Override
			public boolean canUse()
			{
				return super.canUse() && naturalSpawn;
			}
		});
		this.goalSelector.addGoal(10, new LookAtPlayerGoal(this, LivingEntity.class, 8.0F));
		this.targetSelector.addGoal(1, new HurtByTargetGoal(this));
		this.targetSelector.addGoal(2, (new NearestAttackableTargetGoal<Player>(this, Player.class, true)
		{
			@Override
			public boolean canUse()
			{
				List<Raider> nearbyRaiders = this.mob.level().getEntitiesOfClass(Raider.class, new AABB(this.mob.blockPosition()).inflate(20, 10, 20));
				return super.canUse() && nearbyRaiders.size() <= 2;
			}
		}).setUnseenMemoryTicks(300));
		this.targetSelector.addGoal(3, (new NearestAttackableTargetGoal<>(this, AbstractVillager.class, false)).setUnseenMemoryTicks(300));
		this.targetSelector.addGoal(3, new NearestAttackableTargetGoal<>(this, IronGolem.class, false));

		this.goalSelector.addGoal(2, new AvoidEntityGoal<Player>(this, Player.class, 7.0F, 0.6D, 1.0D)
		{
			@Override
			public boolean canUse()
			{
				List<Raider> nearbyRaiders = this.mob.level().getEntitiesOfClass(Raider.class, new AABB(this.mob.blockPosition()).inflate(20, 10, 20));
				return super.canUse() && (nearbyRaiders.size() <= 2 || this.mob.getTarget() != null);
			}
		});

		this.goalSelector.addGoal(8, new WaterAvoidingRandomStrollGoal(this, 0.6D)
		{
			@Override
			public boolean canUse()
			{
				return !naturalSpawn && super.canUse();
			}
		});
	}

	public static AttributeSupplier.Builder registerAttributes()
	{
		return Monster.createMonsterAttributes().add(Attributes.MOVEMENT_SPEED, 0.5D).add(Attributes.FOLLOW_RANGE, 25.0D).add(Attributes.MAX_HEALTH, 120.0D);
	}

	@Override
	public int getAmbientSoundInterval()
	{
		return 120;
	}

	@Override
	public SpawnGroupData finalizeSpawn(ServerLevelAccessor level, DifficultyInstance difficultyIn, EntitySpawnReason reason, @Nullable SpawnGroupData spawnDataIn)
	{
		return super.finalizeSpawn(level, difficultyIn, reason, spawnDataIn);
	}

	@Override
	public void tick()
	{
		super.tick();

		if (this.level().isClientSide() && this.isCastingSpell())
		{
			float f = this.yBodyRot * Mth.DEG_TO_RAD + Mth.cos((float) this.tickCount * 0.6662F) * 0.25F;
			float f1 = Mth.cos(f);
			float f2 = Mth.sin(f);

			if (this.tickCount % 4 == 0)
			{
				double x = this.random.nextGaussian() * 0.03D;
				double y = 0.08D;
				double z = this.random.nextGaussian() * 0.03D;

				ParticleOptions particle = this.random.nextBoolean() ? TCParticles.BLACK_PLAYING_CARD : TCParticles.RED_PLAYING_CARD;
				this.level().addParticle(particle, this.getX() + (double) f1 * 0.6D, this.getY() + 1.8D, this.getZ() + (double) f2 * 0.6D, x, y, z);
				this.level().addParticle(particle, this.getX() - (double) f1 * 0.6D, this.getY() + 1.8D, this.getZ() - (double) f2 * 0.6D, x, y, z);
			}

			float r = this.getSpellColors().getX() / 255.0F;
			float g = this.getSpellColors().getY() / 255.0F;
			float b = this.getSpellColors().getZ() / 255.0F;

			this.level().addParticle(ColorParticleOption.create(ParticleTypes.ENTITY_EFFECT, r, g, b), this.getX() + (double) f1 * 0.6D, this.getY() + 1.8D, this.getZ() + (double) f2 * 0.6D, 0, 0, 0);
			this.level().addParticle(ColorParticleOption.create(ParticleTypes.ENTITY_EFFECT, r, g, b), this.getX() - (double) f1 * 0.6D, this.getY() + 1.8D, this.getZ() - (double) f2 * 0.6D, 0, 0, 0);
		}

		if (this.throwingCardCooldown > 0)
			--this.throwingCardCooldown;
	}

	@Override
	public void readAdditionalSaveData(CompoundTag compound)
	{
		super.readAdditionalSaveData(compound);
		this.naturalSpawn = !compound.contains("IsNaturalSpawn") ? true : compound.getBoolean("IsNaturalSpawn");
	}

	@Override
	public void addAdditionalSaveData(CompoundTag compound)
	{
		super.addAdditionalSaveData(compound);
		compound.putBoolean("IsNaturalSpawn", this.naturalSpawn);
	}

	private Vec3i getSpellColors()
	{
		IllagerSpell spellType = this.getCurrentSpell();

		// bouncy balls 73
		if (spellType == IllagerSpell.FANGS)
			return BOUNCY_BALL_SPELL_COLOR;
		// rabbit 74
		else if (spellType == IllagerSpell.SUMMON_VEX)
			return RABBIT_SPELL_COLOR;
		// displace 75
		else if (spellType == IllagerSpell.WOLOLO)
			return DISPLACE_SPELL_COLOR;
		// disappear
		else if (spellType == IllagerSpell.DISAPPEAR)
			return DISAPPEAR_SPELL_COLOR;
		else
			return Vec3i.ZERO;
	}

	@Override
	public void die(DamageSource cause)
	{
		super.die(cause);

		if (this.level().isClientSide())
		{
			for (int i = 0; i < 20; ++i)
			{
				double d0 = this.random.nextGaussian() * 0.02D;
				double d1 = 0.25D;
				double d2 = this.random.nextGaussian() * 0.02D;

				ParticleOptions particle = this.random.nextBoolean() ? TCParticles.BLACK_PLAYING_CARD : TCParticles.RED_PLAYING_CARD;
				this.level().addParticle(particle, this.getX(1.0D) - d0 * 10.0D, this.getRandomY() - d1 * 10.0D + 1, this.getRandomZ(1.0D) - d2 * 10.0D, d0, d1, d2);
			}
		}
	}

	@Override
	protected boolean considersEntityAsAlly(Entity entity)
	{
		return super.considersEntityAsAlly(entity);
	}

	@Override
	public float getVoicePitch()
	{
		return (this.random.nextFloat() - this.random.nextFloat()) * 0.1F + 1F;
	}

	@Override
	protected SoundEvent getAmbientSound()
	{
		return TCSounds.ENTITY_CONJURER_IDLE;
	}

	@Override
	protected SoundEvent getHurtSound(DamageSource damageSourceIn)
	{
		return TCSounds.ENTITY_CONJURER_HURT;
	}

	@Override
	protected SoundEvent getDeathSound()
	{
		return TCSounds.ENTITY_CONJURER_DEATH;
	}

	@Override
	protected SoundEvent getCastingSoundEvent()
	{
		return TCSounds.ENTITY_CONJURER_CAST_SPELL;
	}

	@Override
	public boolean isPatrolLeader()
	{
		return false;
	}

	@Override
	public void applyRaidBuffs(ServerLevel level, int wave, boolean unused)
	{
	}

	@Override
	public SoundEvent getCelebrateSound()
	{
		return TCSounds.ENTITY_CONJURER_CELEBRATE;
	}

	@Override
	public AbstractIllager.IllagerArmPose getArmPose()
	{
		if (this.getMainHandItem().is(TCRegistry.ItemReg.THROWING_CARD.get()))
			return IllagerArmPose.CROSSBOW_HOLD;
		else
			return super.getArmPose();
	}

	@Override
	public boolean hurtServer(ServerLevel level, DamageSource source, float amount)
	{
		return source.is(DamageTypeTags.BYPASSES_INVULNERABILITY) ? super.hurtServer(level, source, amount) : source.isCreativePlayer() ? super.hurtServer(level, source, amount) : super.hurtServer(level, source, Math.min(15, amount));
	}

	public boolean isEntityLooking(LivingEntity entity)
	{
		if (entity != null)
		{
			Vec3 vec3d = entity.getViewVector(1.0F).normalize();
			Vec3 vec3d1 = new Vec3(this.getX() - entity.getX(), this.getBoundingBox().minY + (double) this.getEyeHeight() - (entity.getY() + (double) entity.getEyeHeight()), this.getZ() - entity.getZ());
			double d0 = vec3d1.length();
			vec3d1 = vec3d1.normalize();
			double d1 = vec3d.dot(vec3d1);

			if (d1 < 1.0D / d0)
			{
				return false;
			}
			else
			{
				return true;
			}
		}
		else
		{
			return false;
		}
	}

	class CastingSpellGoal extends SpellcasterIllager.SpellcasterCastingSpellGoal
	{
		private final ConjurerEntity conjurer = ConjurerEntity.this;

		private CastingSpellGoal()
		{
			super();
		}

		@Override
		public void tick()
		{
			if (this.conjurer.getTarget() != null)
				this.conjurer.getLookControl().setLookAt(this.conjurer.getTarget(), this.conjurer.getMaxHeadYRot(), this.conjurer.getMaxHeadXRot());
		}
	}

	class DisappearSpellGoal extends SpellcasterIllager.SpellcasterUseSpellGoal
	{
		private final ConjurerEntity conjurer = ConjurerEntity.this;

		private DisappearSpellGoal()
		{
			super();
		}

		@Override
		public boolean canUse()
		{
			return this.conjurer.getTarget() != null && (this.conjurer.hurtTime > 0 || this.conjurer.distanceTo(this.conjurer.getTarget()) < 8.0F) && !this.conjurer.hasEffect(MobEffects.INVISIBILITY) && this.conjurer.isEntityLooking(this.conjurer.getTarget()) && super.canUse();
		}

		// Time the Conjurer is in casting animation
		@Override
		protected int getCastingTime()
		{
			return 30;
		}

		// Time between casts
		@Override
		protected int getCastingInterval()
		{
			return 200;
		}

		@Override
		protected void performSpellCasting()
		{
			this.conjurer.addEffect(new MobEffectInstance(MobEffects.INVISIBILITY, 120 + (this.conjurer.getRandom().nextInt(3) * 10), 0));
			this.conjurer.playSound(TCSounds.ENTITY_CONJURER_DISAPPEAR, 1.0F, 1.0F);
			this.conjurer.spawnAnim();

			if (this.conjurer.getTarget() == null)
				return;

			if (this.conjurer.getRandom().nextFloat() <= 0.50F && this.conjurer.distanceTo(this.conjurer.getTarget()) < 7.0F || this.conjurer.hurtTime > 0 || this.conjurer.distanceTo(this.conjurer.getTarget()) <= 4.0F)
			{
				this.conjurer.moveTo(this.conjurer.getTarget().getX(), this.conjurer.getTarget().getY(), this.conjurer.getTarget().getZ(), this.conjurer.getTarget().getYHeadRot(), this.conjurer.getTarget().getXRot());
				this.conjurer.moveRelative(2.0F, new Vec3(0, 0, -4));
			}
		}

		@Override
		protected SoundEvent getSpellPrepareSound()
		{
			return TCSounds.ENTITY_CONJURER_PREPARE_VANISH;
		}

		@Override
		protected SpellcasterIllager.IllagerSpell getSpell()
		{
			return SpellcasterIllager.IllagerSpell.DISAPPEAR;
		}
	}

	class BouncyBallSpellGoal extends SpellcasterIllager.SpellcasterUseSpellGoal
	{
		private final ConjurerEntity conjurer = ConjurerEntity.this;

		private BouncyBallSpellGoal()
		{
			super();
		}

		@Override
		public boolean canUse()
		{
			return super.canUse();
		}

		@Override
		protected int getCastingTime()
		{
			return 40;
		}

		@Override
		protected int getCastingInterval()
		{
			return 220;
		}

		@Override
		protected void performSpellCasting()
		{
			double x = this.conjurer.getTarget().getX() - this.conjurer.getX();
			double y = (this.conjurer.getTarget().getBoundingBox().minY + (this.conjurer.getTarget().getBbHeight() / 2.0F)) - (this.conjurer.getY() + (this.conjurer.getBbHeight() / 2.0F));
			double z = this.conjurer.getTarget().getZ() - this.conjurer.getZ();

			Vec3 lookVector = this.conjurer.getViewVector(1.0F);

			for (int i = 0; i < 5; ++i)
			{
				BouncingBallEntity projectile = new BouncingBallEntity(TCEntityTypes.BOUNCING_BALL, this.conjurer, this.conjurer.level(), TCRegistry.ItemReg.THROWABLE_BALL.toStack());

				RandomSource rand = this.conjurer.getRandom();
				projectile.setNoGravity(!this.conjurer.level().canSeeSky(this.conjurer.blockPosition()));
				projectile.shoot(x + (i * 5) - 10, y + rand.nextInt(3), z, 0.5F, 1.0F);
				projectile.setPos(this.conjurer.getX() + lookVector.x * 1.0D, this.conjurer.getY() + (double) (this.conjurer.getBbHeight() / 2.0F) + 0.2F, this.conjurer.getZ() + lookVector.z * 1.0D);
				projectile.setBallType(rand.nextInt(5));

				if (!this.conjurer.level().isClientSide())
				{
					this.conjurer.level().addFreshEntity(projectile);
				}
			}
		}

		@Override
		protected SoundEvent getSpellPrepareSound()
		{
			return TCSounds.ENTITY_CONJURER_PREPARE_ATTACK;
		}

		@Override
		protected SpellcasterIllager.IllagerSpell getSpell()
		{
			return IllagerSpell.FANGS;
		}
	}

	class BunnySpellGoal extends SpellcasterIllager.SpellcasterUseSpellGoal
	{
		private final ConjurerEntity conjurer = ConjurerEntity.this;

		private BunnySpellGoal()
		{
			super();
		}

		@Override
		public boolean canUse()
		{
			List<Rabbit> nearbyRabbits = this.conjurer.level().getEntitiesOfClass(Rabbit.class, new AABB(this.conjurer.blockPosition()).inflate(25, 15, 25));

			if (!super.canUse() || this.conjurer.getTarget() == null || this.conjurer.hasEffect(MobEffects.INVISIBILITY))
			{
				return false;
			}
			else
			{
				return super.canUse() && nearbyRabbits.size() < 3;
			}
		}

		@Override
		protected int getCastingTime()
		{
			return 50;
		}

		@Override
		protected int getCastingInterval()
		{
			return 300;
		}

		@Override
		protected void performSpellCasting()
		{
			Vec3 lookVector = this.conjurer.getViewVector(1.0F);
			double x = this.conjurer.getX() + lookVector.x;
			double y = this.conjurer.getY() + (double) (this.conjurer.getBbHeight() / 2.0F + 2.0D);
			double z = this.conjurer.getZ() + lookVector.z;
			BlockPos blockpos = BlockPos.containing(x, y, z);
			Rabbit rabbit = EntityType.RABBIT.create(this.conjurer.level(), EntitySpawnReason.MOB_SUMMONED);

			rabbit.goalSelector.addGoal(1, new MeleeAttackGoal(rabbit, 1.4D, true));
			rabbit.targetSelector.addGoal(1, new HurtByTargetGoal(rabbit));
			rabbit.targetSelector.addGoal(2, new NearestAttackableTargetGoal<>(rabbit, Player.class, true));
			rabbit.targetSelector.addGoal(3, new NearestAttackableTargetGoal<>(rabbit, Villager.class, true));

			rabbit.getAttribute(Attributes.ARMOR).setBaseValue(8.0D);
			rabbit.getAttribute(Attributes.MAX_HEALTH).setBaseValue(10.0D);
			rabbit.getAttribute(Attributes.MOVEMENT_SPEED).setBaseValue(0.45D);

			rabbit.setTarget(this.conjurer.getTarget());
			EntityAccessHelper.setDeathLootTable(rabbit, TCRegistry.LootTableReg.HAT_RABBIT);

			rabbit.setVariant(Rabbit.Variant.byId(random.nextBoolean() ? 3 : 1));
			rabbit.setDeltaMovement(rabbit.getDeltaMovement().x(), 0.3F, rabbit.getDeltaMovement().z());
			rabbit.moveTo(blockpos, this.conjurer.yHeadRot, this.conjurer.getXRot());
			rabbit.moveRelative(0.3F, new Vec3(0, 0, 1));
			rabbit.setHealth(10);

			if (!rabbit.isAlliedTo(this.conjurer) && this.conjurer.getTeam() != null)
				this.conjurer.level().getScoreboard().addPlayerToTeam(rabbit.getUUID().toString(), this.conjurer.level().getScoreboard().getPlayerTeam(this.conjurer.getTeam().getName()));

			this.conjurer.level().addFreshEntity(rabbit);
		}

		@Override
		protected SoundEvent getSpellPrepareSound()
		{
			return TCSounds.ENTITY_CONJURER_PREPARE_RABBIT;
		}

		@Override
		protected SpellcasterIllager.IllagerSpell getSpell()
		{
			return IllagerSpell.SUMMON_VEX;
		}
	}

	class DisplaceSpellGoal extends SpellcasterIllager.SpellcasterUseSpellGoal
	{
		private final ConjurerEntity conjurer = ConjurerEntity.this;

		private DisplaceSpellGoal()
		{
			super();
		}

		@Override
		public boolean canUse()
		{
			if (this.conjurer.getTarget() != null && this.conjurer.hasEffect(MobEffects.INVISIBILITY))
			{
				return super.canUse();
			}
			else
			{
				return false;
			}

		}

		// Time the Conjurer is in casting animation
		@Override
		protected int getCastingTime()
		{
			return 60;
		}

		// Time between casts
		@Override
		protected int getCastingInterval()
		{
			return 460;
		}

		@Override
		protected void performSpellCasting()
		{
			LivingEntity entityLiving = this.conjurer.getTarget();
			this.teleportEntity(entityLiving, true);
			// this.teleportEntity(conjurer, true);
		}

		public void teleportEntity(LivingEntity entityLiving, boolean strict)
		{
			Level worldIn = entityLiving.level();
			double d0 = entityLiving.getX();
			double d1 = entityLiving.getY();
			double d2 = entityLiving.getZ();

			for (int i = 0; i < 30; ++i)
			{
				double d3 = entityLiving.getX() + (entityLiving.getRandom().nextDouble() - 0.5D) * 16.0D;
				double d4 = Mth.clamp(entityLiving.getY() + (double) (entityLiving.getRandom().nextInt(5) - 2), 0.0D, (double) (worldIn.getMaxY() - 1));
				double d5 = entityLiving.getZ() + (entityLiving.getRandom().nextDouble() - 0.5D) * 16.0D;

				if (entityLiving.isPassenger())
					entityLiving.stopRiding();

				boolean isProperBlock;

				if (!this.conjurer.level().canSeeSky(this.conjurer.blockPosition()))
				{
					BlockPos pos = BlockPos.containing(d3, d4, d5);

					if (strict)
						isProperBlock = worldIn.getBlockState(pos.below()).getBlock() == Blocks.BIRCH_PLANKS || worldIn.getBlockState(pos.below()).getBlock() == Blocks.STRIPPED_DARK_OAK_LOG || worldIn.getBlockState(pos.below()).getBlock() == Blocks.STRIPPED_DARK_OAK_WOOD || worldIn.getBlockState(pos.below()).getBlock() == Blocks.BIRCH_STAIRS;
					else
						isProperBlock = !worldIn.canSeeSkyFromBelowWater(pos);
				}
				else
					isProperBlock = true;

				if (isProperBlock && entityLiving.randomTeleport(d3, d4, d5, true))
				{
					worldIn.playSound(null, d0, d1, d2, SoundEvents.CHORUS_FRUIT_TELEPORT, SoundSource.PLAYERS, 1.0F, 1.0F);
					entityLiving.playSound(SoundEvents.CHORUS_FRUIT_TELEPORT, 1.0F, 1.0F);
					break;
				}
			}

		}

		@Override
		protected SoundEvent getSpellPrepareSound()
		{
			return TCSounds.ENTITY_CONJURER_PREPARE_DISPLACEMENT;
		}

		@Override
		protected SpellcasterIllager.IllagerSpell getSpell()
		{
			return IllagerSpell.WOLOLO;
		}
	}

	class ThrowingCardAttackGoal extends Goal
	{
		private final ConjurerEntity conjurer = ConjurerEntity.this;
		public int cardThrows = 0;

		private ThrowingCardAttackGoal()
		{
			super();
			this.setFlags(EnumSet.of(Goal.Flag.MOVE, Goal.Flag.LOOK));
		}

		@Override
		public boolean canUse()
		{
			return this.conjurer.throwingCardCooldown <= 0 && this.cardThrows < 10 && !this.conjurer.isCastingSpell() && this.conjurer.getTarget() != null && this.conjurer.getTarget().getRandom().nextInt(20) == 0/* && this.conjurer.getDistance(this.conjurer.getAttackTarget()) > 10.0F*/;
		}

		@Override
		public void stop()
		{
			super.stop();

			this.cardThrows = 0;

			this.conjurer.setItemSlot(EquipmentSlot.MAINHAND, ItemStack.EMPTY);
		}

		@Override
		public boolean canContinueToUse()
		{
			return this.conjurer.getTarget() != null && !this.conjurer.isCastingSpell() && this.cardThrows < 10 && this.conjurer.throwingCardCooldown <= 0;
		}

		@Override
		public void start()
		{
			super.start();

			this.conjurer.setItemSlot(EquipmentSlot.MAINHAND, TCRegistry.ItemReg.THROWING_CARD.toStack());
		}

		@Override
		public void tick()
		{
			super.tick();

			ConjurerEntity conjurer = this.conjurer;

			if (conjurer.tickCount % 4 == 0 && cardThrows < 10)
			{
				conjurer.swing(InteractionHand.MAIN_HAND);
				++cardThrows;

				var target = this.conjurer.getTarget();
				double x = target.getX() - this.conjurer.getX();
				double y = (target.getBoundingBox().minY + (target.getBbHeight())) - (this.conjurer.getY() + (this.conjurer.getBbHeight() / 2.0F));
				double z = target.getZ() - this.conjurer.getZ();

				Vec3 lookVector = this.conjurer.getViewVector(1.0F);

				ThrowingCardEntity projectile = new ThrowingCardEntity(TCEntityTypes.THROWING_CARD, this.conjurer, this.conjurer.level(), TCRegistry.ItemReg.THROWING_CARD.toStack());

				RandomSource rand = this.conjurer.getRandom();
				projectile.shoot(x, y, z, 1.2F, 0.5F);
				projectile.setPos(this.conjurer.getX() + lookVector.x * 1.0D, this.conjurer.getY() + (double) (this.conjurer.getBbHeight() / 2.0F) + 0.3F, this.conjurer.getZ() + lookVector.z * 1.0D);

				conjurer.playSound(SoundEvents.BOOK_PAGE_TURN, 1.0F, 1.0F);

				projectile.setCardType((rand.nextFloat() < 0.1F ? 2 : rand.nextInt(2)));
				if (!this.conjurer.level().isClientSide())
				{
					this.conjurer.level().addFreshEntity(projectile);
				}

				if (cardThrows == 9)
				{
					// this.conjurer.setItemSlot(EquipmentSlot.MAINHAND, ItemStack.EMPTY);
					conjurer.throwingCardCooldown = 200;
				}
			}

			if (this.conjurer.getTarget() != null)
			{
				this.conjurer.getLookControl().setLookAt(this.conjurer.getTarget(), (float) this.conjurer.getMaxHeadYRot(), (float) this.conjurer.getMaxHeadXRot());
			}
		}

		@Override
		public boolean isInterruptable()
		{
			return false;
		}

		@Override
		public boolean requiresUpdateEveryTick()
		{
			return true;
		}
	}
}